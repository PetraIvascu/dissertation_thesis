"""
process_results.py module processes pickle files after training ends
"""
import os
import glob
import math
import numpy as np
from utils.utils import read_pickled_results, pickle_results
from utils.utils import read_pickled_results_est


def read_files_txt(location):
  """reads some data from txt files to provide dictionary with
  min/max/avg accuracy per rounds of co-training

  Arguments:
      location {string} -- path to where txt files  are

  Returns:
      dictionary -- min/max/avg accuracy per rounds of co-training
  """
  results = {}
  files = sorted(glob.glob("{}\*.txt".format(location)))
  # check order files
  for file_name in files:
    key = os.path.basename(file_name).split("-", 1)[0]
    added_pseudo_labels_exp = []
    rounds_acc_exp = []
    fo = open(file_name, "r")
    lines = fo.readlines()
    for i, _ in enumerate(lines):
      #if i != 0 and i % 2 == 0:
      if lines[i].split()[0] is not 'time' and lines[i].split()[0].isdigit():
        added_pseudo_labels_exp.append(int(lines[i].split()[4]))
        rounds_acc_exp.append(float(lines[i].split()[-1]))
    # Fake labels
    np_added_pseudo_labels_exp = np.array(added_pseudo_labels_exp)
    np_added_pseudo_labels_exp = np.reshape(np_added_pseudo_labels_exp,
                                            (10, 20))  # 20 c256
    # ACC rounds
    np_rounds_acc_exp = np.array(rounds_acc_exp)
    np_rounds_acc_exp = np.reshape(np_rounds_acc_exp, (10, 20))  # 20 c256

    avg_added_pseudo_labels_exp = np.mean(np_added_pseudo_labels_exp, axis=0)
    min_added_pseudo_labels_exp = np.min(np_added_pseudo_labels_exp, axis=0)
    max_added_pseudo_labels_exp = np.max(np_added_pseudo_labels_exp, axis=0)

    avg_rounds_acc_exp = np.mean(np_rounds_acc_exp, axis=0)

    # added_pseudo_labels_all.append(added_pseudo_labels_exp)
    results[key] = [
        avg_rounds_acc_exp, avg_added_pseudo_labels_exp,
        min_added_pseudo_labels_exp, max_added_pseudo_labels_exp
    ]

  return results


def file_aggregator(location):
  """ aggragates files

  Arguments:
      location {string} -- location of pkl files

  Returns:
      dictionary -- level of supervision as key and value paths correspondent
  """
  # format name pkl file: {0-9}-supervision-something.pkl
  results_files = {}
  files = glob.glob("{}\*.pkl".format(location))
  for f in sorted(files):
    split_name = os.path.basename(f).rsplit('-', 6)
    if len(results_files) == 0 or (split_name[1] not in list(
        results_files.keys())):
      results_files[split_name[1]] = [f]
    else:
      results_files[split_name[1]].append(f)

  return results_files


def get_final_results_join_estimator(dict_file_aggregated,
                                     experiment='baseline'):
  """aggregates different experiments and reads values of them

  Arguments:
      dict_file_aggregated {dictionary} -- lvl sup and correspondentent paths

  Keyword Arguments:
      experiment {str} -- type of experiments (default: {'baseline'})

  Returns:
      dictionary -- values are values experiments attached to a key representing
      level of supervision
  """
  # aggregates only the last co-training round results
  dict_results = {}
  for key, value in dict_file_aggregated.items():
    all_metrics_round = []
    for v in value:
      if experiment == 'baseline':
        metrics = read_pickled_results_est(v)
      else:
        metrics = read_pickled_results(v)

      all_metrics_round.append(metrics)
    dict_results[key] = all_metrics_round
  return dict_results  # {k:[[],[],...,[]]}


def get_avg_accuracy_rounds(dict_all_results):
  """returns avg accuracy from experiments

  Arguments:
      dict_all_results {dictionary} -- all data from experiments

  Returns:
      dictionary -- accurcay per levels of supervision
  """
  results = {}
  for key, value in dict_all_results.items():
    arr_value = np.array(value)
    results[key] = np.mean(arr_value, axis=1)
  return results


def average_results(dict_all_results):
  """computes average of metrics

  Arguments:
     dict_all_results {dictionary} -- all data from experiments

  Returns:
      dictionary -- average values metrics per levels of supervision
  """
  results = {}
  for key, value in dict_all_results.items():
    arr_value = np.array(value)
    results[key] = np.mean(arr_value, axis=0)
  return results


def std_results(dict_all_results):
  """computes standard deviation for information from experiments

  Arguments:
      dict_all_results {dictionary} -- all data from experiments

  Returns:
      dictionary -- std values metrics per levels of supervision
  """
  results = {}
  for key, value in dict_all_results.items():
    arr_value = np.array(value)
    results[key] = np.std(arr_value, axis=0)
  return results


def min_results(dict_all_results):
  """computes min deviation for information from experiments

  Arguments:
      dict_all_results {dictionary} -- all data from experiments

  Returns:
      dictionary -- min values metrics per levels of supervision
  """
  results = {}
  for key, value in dict_all_results.items():
    arr_value = np.array(value)
    results[key] = np.min(arr_value, axis=0)
  return results


def max_results(dict_all_results):
  """computes max deviation for information from experiments

  Arguments:
      dict_all_results {dictionary} -- all data from experiments

  Returns:
      dictionary -- max values metrics per levels of supervision
  """
  results = {}
  for key, value in dict_all_results.items():
    arr_value = np.array(value)
    results[key] = np.max(arr_value, axis=0)
  return results


def compute_confidance_intervals(dict_results, test_size):
  """computes confidance intervals
  1.96 - 95% confidance level
  interval = z * sqrt( (accuracy * (1 - accuracy)) / n)
  Arguments:
      dict_all_results {dictionary} -- all data from experiments
      test_size {float} -- test size used

  Returns:
      dictionary -- levels of confidance  per lvl of supervision
  """

  intervals = {}
  for key, _ in dict_results.items():
    acc = dict_results[key][0]
    intervals[key] = 1.96 * math.sqrt((acc * (1 - acc)) / test_size)

  return intervals


def main(location_pkl, location_txt, test_size, pkl_file, experiment_type):
  """aggregates multiple methods to create results of processing

  Arguments:
      location_pkl {string} -- path to pickle files
      location_txt {string} -- path to txt files
      test_size {float32} -- test size used
      pkl_file {string} -- path to the finisk pickle file
      experiment_type {string} -- type of experiment baseline/co-training
  """
  summary_pseudo_labels = read_files_txt(location_txt)
  files = file_aggregator(location_pkl)
  results = get_final_results_join_estimator(files, experiment=experiment_type)
  #avg_rounds_dict = get_avg_accuracy_rounds(results)
  avg_dict = average_results(results)
  std_dict = std_results(results)
  min_dict = min_results(results)
  max_dict = max_results(results)
  confidance_intervals = compute_confidance_intervals(avg_dict, test_size)
  merge_dict = {}
  for k, _ in avg_dict.items():
    merge_dict[k] = [
        avg_dict[k], min_dict[k], max_dict[k], confidance_intervals[k],
        std_dict[k], summary_pseudo_labels[k]
    ]

  pickle_results(pkl_file, merge_dict)


if __name__ == "__main__":

  dir_name = 'Transductive-MAE-Resnet'
  dataset = 'caltech_256'
  pkl_finish_name = 'results_mae_resnet_metrics_co_tr_TRANSDUCTIVE_processed.pkl'
  test_size = 3318  # c256 - 3318 --- C101 - 1017
  exp_type = 'co_training'  #'co_training', 'baseline'

  location_pkl = os.path.join(
      os.path.dirname(os.path.realpath(__file__)), 'output', dataset, dir_name,
      'pkl')
  location_txt, _ = os.path.split(location_pkl)
  pkl_file_finish = os.path.join(
      os.path.dirname(os.path.realpath(__file__)), 'output',
      'results_processed', dataset, pkl_finish_name)

  main(
      location_pkl,
      location_txt,
      test_size,
      pkl_file_finish,
      experiment_type=exp_type)
