"""
  controller_view.py module responsible with basic operations inter view
  instances
"""

import numpy as np
from query_selector import UncertaintySampler


class ControllerView:
  """
    ControllerView provides basic interactions between views
  """

  def concatenate_views(self, v1, v2, labels):
    """concatenates two views concat(v1, v2) = v1v2

    Arguments:
        v1 {float32} -- numpy array with features to be concatenated
        v2 {float32} -- numpy array with features to be concatenated
        labels {uint16} -- numpy array containing correspondent labels

    Returns:
        float32 -- concat(v1, v2) = v1v2
    """
    # concatenate views
    cp_v1 = np.copy(v1)
    cp_v2 = np.copy(v2)
    cp_lbls = np.copy(labels)
    return np.concatenate((cp_v1, cp_v2), axis=1), cp_lbls

  def union_picked_data(self, indxs_v1, indxs_v2, lbls_v1, lbls_v2):
    """solves conflicts between the proposed labels by estiamtors

    Arguments:
        indxs_v1 {uint32} -- indexes view 1 for features from view 1
        indxs_v2 {uint32} -- indexes view 2 for features from view 2
        lbls_v1 {uint16} -- labels of instances view 1
        lbls_v2 {uint16} -- labels of instances view 2

    Returns:
        dictionary -- dictionary containing indexes with pseudo-labels proposed
    """

    dict_v1 = dict(zip(indxs_v1, lbls_v1))
    dict_v2 = dict(zip(indxs_v2, lbls_v2))
    dict_final = {}
    for key in dict_v1:
      if key in dict_v2:
        if dict_v1[key] == dict_v2[key]:
          dict_final[key] = dict_v1[key]
      else:
        dict_final[key] = dict_v1[key]
    for key in dict_v2:
      if key not in dict_v1:
        dict_final[key] = dict_v2[key]

    return dict_final

  def selection_agreement_base(self, predictions_v1, predictions_v2):
    """selection strategy based on agreement for proposed pseudo-labels

    Arguments:
        predictions_v1 {uint16} -- predictions made by estimator 1
        predictions_v2 {uint16} -- predictions made by estimator 2

    Returns:
        uint32 -- indexes where predctions are the same
    """
    # will decide what gets appended labeled data based on agreement
    return np.where(predictions_v1 == predictions_v2)  # indexes

  def selection_confidance_based(self, predictions):
    """selection strategy based on the estiamted confidance for predictions
       closer to 1 -> more uncertant

    Arguments:
        predictions {float32} -- predictions made by softmax layer of estimator

    Returns:
        float32 -- uncertanty score for each prediction made by estimator
    """
    uncertainty_sampler = UncertaintySampler(sampling_type=1)
    uncertainty = uncertainty_sampler.compute_score(predictions)
    return uncertainty

  def select_indxs_asc(self, uncertanty_scores, k):
    """finds correspondent index in feature array for indexes provided by
       uncertanty array

    Arguments:
        uncertanty_scores {floar32} -- numpy array with uncertanty scores
        k {int} -- picks first n smallest scores

    Returns:
        uint64 -- array of indexes mapped to feature array
    """
    selection = []
    for i, _ in enumerate(uncertanty_scores):
      selection.append((i, uncertanty_scores[i]))
    return np.array(
        [x[0] for x in sorted(selection, key=lambda sel: sel[1])[:k]],
        dtype=np.uint64)


if __name__ == "__main__":
  # small test
  cntrl_view = ControllerView()
  scores_uncertanty = np.array([
      0.98941959, 0.48661546, 0.05680162, 0.26408812, 0.61393924, 0.0021234,
      0.93125, 0.87, 0.5, 0.6, 0.234
  ])
  top_k = 10
  indxs_picked = cntrl_view.select_indxs_asc(scores_uncertanty, top_k)
