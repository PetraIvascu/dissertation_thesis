from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import random
import numpy as np
from enum import Enum
import sys
import math

from sklearn import preprocessing


class QuerySelector:

  def select(self, entries, evaluations, no_selections=1):
    if no_selections > len(entries):
      no_selections = len(entries)
    return [entries[x] for x in self.select_indexes(evaluations, no_selections)]

  def select_indexes(self, evaluations, no_selections=1):
    if no_selections > len(evaluations) or no_selections == -1:
      no_selections = len(evaluations)
    selection = []
    for i in range(0, len(evaluations)):
      selection.append((i, self.score(evaluations[i])))
    return [
        x[0] for x in sorted(selection, key=lambda sel: sel[1], reverse=True)
        [:no_selections]
    ]

  def compute_score(self, evaluations):
    return [self.score(ev) for ev in evaluations]

  def score(self, evaluations):
    return random.random()


class UncertaintySampler(QuerySelector):

  def __init__(self, sampling_type=1):
    self.sampling_type = sampling_type

  def score(self, evaluation):

    evaluation_probabilities = sorted(evaluation, reverse=True)

    if evaluation_probabilities[0] > 1:
      print(evaluation_probabilities)
    if self.sampling_type == 1:
      return 1 - evaluation_probabilities[0]
    if self.sampling_type == 2:
      return evaluation_probabilities[1] - evaluation_probabilities[0]
    if self.sampling_type == 3:
      return -sum(
          [x * np.log(x) if x > 0 else 0 for x in evaluation_probabilities])


class CostEffectiveSampler(QuerySelector):

  def __init__(self):
    self.sampling_type = UncertaintySamplingType.ENTROPY

  def select_indexes(self, evaluations, no_selections=2, threshold=0.05):
    selection = []
    for i in range(0, len(evaluations)):
      selection.append((i, self.score(evaluations[i])))
    selection = sorted(selection, key=lambda sel: sel[1], reverse=True)
    print(selection[len(selection) - 10][1])
    k = len(selection) - 1
    while k > len(selection) - no_selections and selection[k][1] < threshold:
      score = selection[k][1]
      print(score)
      k = k - 1
    no_k = no_selections - (len(selection) - k)
    print(no_k)
    if no_k < 0:
      no_k = 0
    sorted_indexes = [x[0] for x in selection]
    print(len(selection) - k, no_k)
    return sorted_indexes[:no_k], sorted_indexes[k:]

  def score(self, evaluation):
    evaluation_probabilities = sorted(evaluation, reverse=True)
    if self.sampling_type == UncertaintySamplingType.LEAST_CONFIDENCE:
      return 1 - evaluation_probabilities[0]
    if self.sampling_type == UncertaintySamplingType.MARGIN_SAMPLING:
      return evaluation_probabilities[1] - evaluation_probabilities[0]
    if self.sampling_type == UncertaintySamplingType.ENTROPY:
      return -sum(
          [x * np.log(x) if x > 0 else 0 for x in evaluation_probabilities])


class DistanceSampler(QuerySelector):

  def __init__(self, entries):
    if entries is None:
      self.no_entries = 0
      self.distances = None
    else:
      self.no_entries = len(entries)
      self.compute_distances(entries)

  def compute_distances(self, entries):
    size = len(entries)
    self.distances = np.zeros(shape=(size, size), dtype=np.float32)
    for i in range(0, size):
      for j in range(i, size):
        self.distances[i, j] = self.distances[j, i] = np.linalg.norm(
            np.array(entries[j]) - np.array(entries[i]))
    normalizer = preprocessing.Normalizer(copy=False).fit(self.distances)
    self.distances = normalizer.transform(self.distances)

  def select(self, entries, labeled_indexes, no_selections=1):
    if no_selections > len(entries):
      no_selections = len(entries)
    return [
        entries[x] for x in self.select_indexes(labeled_indexes, no_selections)
    ]

  def select_indexes(self, labeled_indexes, no_selections=1):
    distances_to_labeled = np.matrix.take(self.distances, labeled_indexes, 1)
    rest_indexes = list(set(range(0, self.no_entries)) - set(labeled_indexes))
    distances_to_labeled = np.matrix.take(distances_to_labeled, rest_indexes, 0)
    return super(DistanceSampler, self).select_indexes(
        evaluations=distances_to_labeled, no_selections=no_selections)


class DensitySampler(DistanceSampler):

  def __init__(self, entries, k=5):
    self.k = k
    if entries is None:
      self.no_entries = 0
      self.distances = None
    else:
      self.no_entries = len(entries)
      self.compute_distances(entries)

  def score(self, distances):
    closest = np.argsort(distances)
    k = self.k
    if self.k > len(closest):
      k = len(closest)
    sum = 0
    for i in range(0, k):
      sum = sum + distances[closest[i]]
    return sum / self.k


class DiversitySampler(DistanceSampler):

  def score(self, distances):
    return np.min(distances)


class HybridSampler(QuerySelector):

  def __init__(self, entries, k=5):
    self.uncertainty_sampler = UncertaintySampler(
        UncertaintySamplingType.MARGIN_SAMPLING)
    self.density_sampler = DensitySampler(entries, k)
    self.diversity_sampler = DiversitySampler(entries)

  def select(self, entries, labeled_indexes, evaluations, no_selections=1):
    if no_selections > len(entries):
      no_selections = len(entries)
    return [
        entries[x] for x in self.select_indexes(labeled_indexes, evaluations,
                                                no_selections)
    ]

  def select_indexes(self, labeled_indexes, evaluations, no_selections=1):
    if no_selections > len(evaluations):
      no_selections = len(evaluations)
    distances_to_labeled = np.matrix.take(self.density_sampler.distances,
                                          list(labeled_indexes), 1)
    rest_indexes = list(
        set(range(0, self.density_sampler.no_entries)) - set(labeled_indexes))
    distances_to_labeled = np.matrix.take(distances_to_labeled, rest_indexes, 0)
    selection = []
    for i in range(0, len(evaluations)):
      selection.append((i, self.score(evaluations[i], distances_to_labeled[i])))
    return [
        x[0] for x in sorted(selection, key=lambda sel: sel[1], reverse=True)
        [:no_selections]
    ]

  def score(self, probabilities, distances):
    return self.uncertainty_sampler.score(probabilities) + self.density_sampler.score(distances) - \
           self.diversity_sampler.score(distances)


class UncertaintySamplingType(Enum):
  LEAST_CONFIDENCE = 1
  MARGIN_SAMPLING = 2
  ENTROPY = 3
