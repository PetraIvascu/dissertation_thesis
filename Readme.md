# Co-training



This repository contains my dissertation thesis on co-training applied on images.
We our experiments were done on the following datasets:
  - [Caltech 256](http://www.vision.caltech.edu/Image_Datasets/Caltech101/) 
  - [Caltech 101](http://www.vision.caltech.edu/Image_Datasets/Caltech256/)

This thesis used pre-trained convolutional neural networks to extract features for the above datasets using [Keras](https://keras.io/). We used following pre-trained networks:
   - [VGG 16](https://arxiv.org/pdf/1409.1556.pdf)
   - [Resnet-101-v2](https://www.cv-foundation.org/openaccess/content_cvpr_2016/papers/He_Deep_Residual_Learning_CVPR_2016_paper.pdf)

For some of the experiments we employed autoencoders in order to compress our features to make the training process faster. This was done using [Tensorflow](https://www.tensorflow.org/). 

Co-training as main features uses views. In order to split single view datasets into multiple views for some experiments we used [Modular Autoencoders](https://arxiv.org/pdf/1511.07340.pdf) and for a more simple take we used for views the plain compressed freatures extracted using the architectures mentioned above. 

### Installation

Install the dependencies.

```sh
$ cd dissertation_thesis
$ pip install -r requirements.txt
```

To run the UI created with dash via plotly you need to create those directories
```sh
$ mkdir output; cd output; mkdir results_processed; cd results_processed
$ mkdir caltech_256; mkdir caltech_101
```
Into the freshly created directory caltech_256 and caltech_101 add pickled files downloaded from this [google drive link](https://drive.google.com/open?id=1RZg7oqOiaoi9ic8ZYamwLHgf2U4Nh1QT).

To start app run this command:
```sh
$ python ui_results_app.py
```
This will lunch the server and in your browser 127.0.0.1:8050, the app can be tested.
















  
