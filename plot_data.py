"""
plot_data.py module helps with creation of different type of plots
"""
import os
import glob
import pickle
import numpy as np
import matplotlib.pyplot as plt

from utils.utils_plot import get_acc_level_supervison, get_acc
from utils.utils_plot import get_map_level_supervison
from utils.utils_plot import get_pseudo_labes_add, get_std_level_supervison

map_to_compare = [0.162, 0.199, 0.245, 0.318, 0.37,
                  0.38]  # last graph including feat alexnet


def plot_acc_fake_labels(acc_fake_labels,
                         precentages_fake_labes,
                         methods,
                         experiment_series,
                         save_figure=True):
  """plots accuracy against #fake labels added per round of co-training

  Arguments:
      acc_fake_labels {list floats} -- avg acc values for each round of co-tr
                                       for each experiment
      precentages_fake_labes {list floats} -- avg value of added labels per
                                    round of co-training for each experiment
      methods {list strings} -- names of the experiments
      experiment_series {string} -- name of the plot

  Keyword Arguments:
      save_figure {bool} -- saves plot (default: {True})
  """

  colours = ['#b56760', '#99ccff', '#666699', '#669999', '#b6bf84']
  markers = ['*', 'D', 'o', '+', 'v']

  ax = plt.subplot(111)
  for i, _ in enumerate(acc_fake_labels):
    ax.plot(
        precentages_fake_labes[i][5],
        acc_fake_labels[i][5],
        label=methods[i],
        color=colours[i],
        marker=markers[i])

    ax.axhline(y=acc_fake_labels[4][5][0], xmax=7000, color=colours[4])

    ax.legend(
        loc='upper center', bbox_to_anchor=(1.2, 0.7), shadow=True, ncol=1)
  # naming the x axis
  plt.xlabel('Number of pseudo-labels added')
  # naming the y axis
  plt.ylabel('Accuracy')
  # giving a title to my graph
  plt.title(experiment_series)

  # save plot if save_figure set True
  if save_figure:
    plt.savefig(
        os.path.join(
            os.path.dirname(os.path.dirname(os.path.realpath(__file__))),
            "figures", experiment_series + "-pseudo-lbls" + ".png"),
        bbox_inches='tight')
  # function to show the plot
  plt.show()


def plot_metric_vs_supervision(metric,
                               precentages_supervision,
                               methods,
                               experiment_series,
                               type_metric='acc',
                               save_figure=True):
  """plots given metric against levels of supervision used for experiments

  Arguments:
      metric {list float} -- list of avg metric over 10 runs for each experiment
      precentages_supervision {list int} -- list of levels of supervision
      methods {list strings} -- names of the experiments
      experiment_series {string} -- name of the plot

  Keyword Arguments:
      save_figure {bool} -- saves plot (default: {True})
  """

  markers = ['*', 'D', 'o', '+', 'v', 'Y']
  colours = ['#669999', '#666699', '#99ccff', '#bddb7b', '#b56760', '#b6bf84']

  if type_metric == 'acc':
    ylabel = 'Accuracy'
  elif type_metric == 'map':
    ylabel = 'MAP'

  for i, _ in enumerate(metric):
    plt.plot(
        precentages_supervision,
        metric[i],
        label=methods[i],
        marker=markers[i],
        color=colours[i])

  plt.xlim(left=1, right=20)
  plt.xticks([2, 3, 5, 10, 15, 20])
  # naming the x axis
  plt.xlabel('Number of images labeled per class', fontsize=15)
  plt.ylim(bottom=0, top=0.87)
  # naming the y axis
  plt.ylabel(ylabel, fontsize=15)
  # giving a title to my graph
  plt.title(experiment_series)

  # show a legend on the plot
  plt.legend()
  # save plot if save_figure set True
  if save_figure:
    plt.savefig(
        os.path.join(
            os.path.dirname(os.path.realpath(__file__)), "figures",
            experiment_series + ".png"))
  # function to show the plot
  plt.show()


def plot_error_bar_metric(avg_metric,
                          precentages_supervision,
                          y_error,
                          methods,
                          experiment_series,
                          save_figure=True):
  """uncertanty of metric given through error bars

  Arguments:
      avg_metric {list float} -- list of avg metric over 10 runs for each
                                 experiment
      precentages_supervision {list int} -- list of levels of supervision
      y_error {list flopats} -- error of y here std
      methods {list strings} -- names of the experiments
      experiment_series {string} -- name of the plot

  Keyword Arguments:
      save_figure {bool} -- saves plot (default: {True})
  """
  colours = ['#b56760', '#99ccff', '#666699', '#669999', '#b6bf84']
  for i, _ in enumerate(avg_metric):
    plt.errorbar(
        precentages_supervision,
        avg_metric[i],
        yerr=np.array(y_error[i]).T,
        label=methods[i],
        color=colours[i],
        uplims=True,
        lolims=True)

  plt.xlim(left=1, right=20)
  plt.xticks([2, 3, 5, 10, 15, 21])
  # naming the x axis
  plt.xlabel('Number of images labeled per class', fontsize=15)
  # naming the y axis
  plt.ylabel('Accuracy', fontsize=15)
  # giving a title to my graph
  plt.title(experiment_series)

  # show a legend on the plot
  plt.legend()
  # save plot if save_figure set True
  if save_figure:
    plt.savefig(
        os.path.join(
            os.path.dirname(os.path.dirname(os.path.realpath(__file__))),
            "figures", experiment_series + " bar-error" + ".png"))
  # function to show the plot
  plt.show()


def make_plot_acc_vs_pseudo_labels(
    all_data_files,
    experiment_series_name,
    methods=[
        "MAE-Resnet", "MAE-VGG", "MAE-VGG-Resnet", "Resnet-VGG", "XGB estimator"
    ],
    save_figure=True):
  """aggreagates all its need it to show fake labels influence upon acc

  Arguments:
      all_data_files {list strings} -- list of paths
      experiment_series_name {string} -- name of the experiment,
      will go as title for chart

  Keyword Arguments:
      methods {list} -- [description]
      (default: {["MAE-Resnet", "MAE-VGG", "MAE-VGG-Resnet", "Resnet-VGG",
       "XGB estimator"]})
      save_figure {bool} -- save the figure or not (default: {True})
  """
  exps_acc = get_acc_level_supervison(all_data_files)
  exps_pseudo_labels = get_pseudo_labes_add(all_data_files)
  plot_acc_fake_labels(
      exps_acc,
      exps_pseudo_labels,
      methods,
      experiment_series_name,
      save_figure=save_figure)


def make_plot_metric_vs_supervision(
    all_data_files,
    experiment_series_name,
    metric='acc',
    supervision=[2, 3, 5, 10, 15, 20],
    methods=[
        "MAE-Resnet", "MAE-VGG", "MAE-VGG-Resnet", "Resnet-VGG", "XGB estimator"
    ],
    save_figure=True):
  """aggreagates all its need it to show supervision influence upon metric given

  Arguments:
      all_data_files {list string} -- list containign all the files
      experiment_series_name {string} -- name of the experiment,
      will go as title for chart

  Keyword Arguments:
      metric  {string} -- what metric to plot (default: acc)
      supervision {list} -- levels of supervision
      (default: {[2, 3, 5, 10, 15, 20]})
      methods {list} -- methods used in experiments
      (default: {["MAE-Resnet", "MAE-VGG", "MAE-VGG-Resnet", "Resnet-VGG",
       "XGB estimator"]})
      save_figure {bool} -- save figure or not (default: {True})
  """
  if metric == 'acc':
    exps_metric = get_acc(all_data_files)
  elif metric == 'map':
    exps_metric = get_map_level_supervison(all_data_files)

  plot_metric_vs_supervision(
      exps_metric,
      supervision,
      methods,
      experiment_series_name,
      type_metric=metric,
      save_figure=save_figure)


def make_plot_error_bar(all_data_files,
                        experiment_series_name,
                        supervision=[2, 3, 5, 10, 15, 20],
                        methods=[
                            "MAE-Resnet", "MAE-VGG", "MAE-VGG-Resnet",
                            "Resnet-VGG", "XGB estimator"
                        ],
                        save_figure=True):
  """aggregates methods to construct plot metric error bar

  Arguments:
      all_data_files {list string} -- names of the files
      experiment_series_name {string} -- name of experiment

  Keyword Arguments:
      supervision {list} -- levels of supervision
      (default: {[2, 3, 5, 10, 15, 20]})
      methods {list} -- methods used in experiments
      (default: {["MAE-Resnet", "MAE-VGG", "MAE-VGG-Resnet", "Resnet-VGG",
       "XGB estimator"]})
      save_figure {bool} -- save the figure or not (default: {True})
  """
  exps_std = get_std_level_supervison(all_data_files)
  exps_avg_acc = get_acc(all_data_files)
  plot_error_bar_metric(
      exps_avg_acc,
      supervision,
      y_error=exps_std,
      experiment_series=experiment_series_name,
      methods=methods,
      save_figure=save_figure)


if __name__ == "__main__":
  proc_results = os.path.join(
      os.path.dirname(os.path.realpath(__file__)), 'output',
      'results_processed', 'caltech_101', 'comparison')
  exp_serie_name = "Caltech 101"

  files = glob.glob("{}\*.pkl".format(proc_results))
  data_files = []
  for f in files:
    with open(f, 'rb') as pklf:
      res = pickle.load(pklf)
    data_files.append(res)

  # fake labels influencing accuracy
  # make_plot_acc_vs_pseudo_labels(data_files, exp_serie_name, save_figure=False)

  # plots metric=acc vs supervision
  # make_plot_metric_vs_supervision(data_files, exp_serie_name, save_figure=False)

  # plots metric=map vs supervision
  # make_plot_metric_vs_supervision(
  #     data_files, exp_serie_name, metric='map', save_figure=False)

  # error bar plot
  # make_plot_error_bar(data_files, exp_serie_name, save_figure=False)

  # comaparison C256
  # to_compare = [0.295, 0.325, 0.350, 0.370, 0.38, 0.39, 0.395, 0.4, 0.41, 0.42]
  # acc_best = get_acc(data_files)
  # methods = ["MAE-Resnet", "McPMVC"]
  # new_exps_acc_best_compare = [acc_best[0], to_compare]
  # plot_metric_vs_supervision(
  #     new_exps_acc_best_compare, [5, 10, 15, 20, 25, 30, 35, 40, 45, 50],
  #     methods,
  #     exp_serie_name,
  #     type_metric='acc',
  #     save_figure=True)

  # comp 2 c101
  to_compare = [0.725, 0.78, 0.81, 0.84, 0.849, 0.85]
  best_mine = get_map_level_supervison(data_files)
  methods = ["VGG-Resnet", "CURL"]
  new_exps_acc_best_compare = [best_mine[0], to_compare]
  plot_metric_vs_supervision(
      new_exps_acc_best_compare, [2, 3, 5, 10, 15, 20],
      methods,
      exp_serie_name,
      type_metric='map',
      save_figure=True)
