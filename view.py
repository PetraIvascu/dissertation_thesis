"""
view.py module provides basic functionalities for a view
"""
import numpy as np


class View:
  """
    encapsulates a series of properties specific view from co-training
  """

  def __init__(self,
               ids_labeled,
               ids_unlabeled,
               train_features,
               train_labels,
               test_features=None,
               test_labels=None):

    self.ids_unlabeled = ids_unlabeled
    self.ids_labeled = ids_labeled
    self.train_features = train_features
    self.train_labels = train_labels
    self.test_features = test_features
    self.test_labels = test_labels
    self.ids_picked = np.array([], dtype=np.uint64)
    self.labels_picked = np.array([], dtype=np.uint16)

  def get_ids_unlabeled_by_id_scores(self, dictionary_asociations, ids_scores):
    """top k best scores per class

    Arguments:
        dictionary_asociations {dictionary} --
        ids_scores {uint64} -- indexes for uncertanty scores

    Returns:
        [uint64] -- top k best scores per class
    """

    ids_unlabeled_top_k = []
    for id_score in ids_scores:
      ids_unlabeled_top_k.append(dictionary_asociations[id_score])

    return np.asarray(ids_unlabeled_top_k, dtype=np.uint64)

  def get_features_by_ids(self, ids):
    """gives features correspondent for given ids

    Arguments:
        ids {uint64} -- ids for features

    Returns:
        float32 -- features corespondent to ids
    """
    return self.train_features[ids]

  def remove_unlabeled_ids(self, ids_to_remove):
    """removes ids from unalbeled list of ids

    Arguments:
        ids_to_remove {uint64} -- array with ids to be removed
    """
    bool_mask = np.logical_not(np.isin(self.ids_unlabeled, ids_to_remove))
    self.ids_unlabeled = self.ids_unlabeled[bool_mask]

  def append_picked_ids(self, arr_1):
    """adds ids picked by selection process to a list
    Arguments:
        arr_1 {uint64} -- array with selected ids
    """
    self.ids_picked = np.append(self.ids_picked, arr_1)

  def append_picked_labels(self, arr_1):
    """adds labels picked by selection process to a list

    Arguments:
        arr_1 {uint32} -- array with labels to append
    """
    self.labels_picked = np.append(self.labels_picked, arr_1)

  def append_unlabeled_ids(self, arr_1):
    """adds ids picked to labeled pool of ids
    Arguments:
        arr_1 {uint64} -- array with selected ids
    """
    self.ids_labeled = np.append(self.ids_labeled, arr_1)

  def append_fake_labels(self, arr_1):
    """adds labels picked to labeled pool of ids

    Arguments:
        arr_1 {uint32} -- array with labels to append
    """
    self.labels_labeled = np.append(self.labels_labeled, arr_1)

  def get_top_k_per_class(self, predictions, scores, k):
    """pickes the best n items per each existing class

    Arguments:
        predictions {float32} -- prediction array from softmax layer estimator
        scores {float32} -- array uncertanty scores
        k {int} -- how many elements to select per class

    Returns:
        uint64 -- array of indexes
    """

    result_global = np.array([], dtype=np.uint64)
    np_scores = np.asarray(scores, dtype=np.float64)
    unq_labels_pred = np.unique(predictions)

    assert predictions.shape[0] == np_scores.shape[0]

    for elem in unq_labels_pred:
      indxs = np.where(elem == predictions)[0]
      indxs_scores = np_scores[indxs]
      pairs = zip(indxs, indxs_scores)
      result = np.array(
          [x[0] for x in sorted(pairs, key=lambda sel: sel[1])[:k]],
          dtype=np.uint64)
      result_global = np.append(result_global, result)

    return result_global
