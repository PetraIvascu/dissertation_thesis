"""
co-training-exchange.py module trains estiamtors in a co-training fashion
"""

import os
import sys
import time
import logging
import numpy as np
from sklearn.utils.class_weight import compute_sample_weight
from sklearn.model_selection import train_test_split

from xgboost_estimator import XGBoostEstimator
from view import View
from controller_view import ControllerView
from split_level_supervision import get_level_supervision_indxs

from utils.utils import write_to_file, read_checkpoint_file
from utils.utils import shuffle_unison, load_compressed_features
from utils.utils import pickle_results, compute_metrics, get_class_weights

where, _ = os.path.split(os.path.realpath(__file__))
sys.path.append(where)

from configs import configs


def append_results_dict(result_dict, metrics, supervision, view):

  l = [s for s in list(result_dict.keys()) if view in s]

  if len(result_dict) == 0 or len(l) == 0:
    result_dict['id'] = supervision
    result_dict['acc_' + view] = [metrics[0]]
    result_dict['err_' + view] = [metrics[1]]
    result_dict['p_' + view] = [metrics[2]]
    result_dict['r_' + view] = [metrics[3]]
    result_dict['m_ap_' + view] = [metrics[4]]
  else:
    result_dict['acc_' + view].append(metrics[0])
    result_dict['err_' + view].append(metrics[1])
    result_dict['p_' + view].append(metrics[2])
    result_dict['r_' + view].append(metrics[3])
    result_dict['m_ap_' + view].append(metrics[4])


def split_data(features, labels, test_size, shuffle=True):
  """splits data into trainand validation

  Arguments:
      features {float32} -- array containing features
      labels {uint32} -- array containing correspondent labels
      test_size {flor32} -- scalar, the test size picked

  Keyword Arguments:
      shuffle {bool} -- if data to be shuffled (default: {True})

  Returns:
      float32 -- training and validation features
      uint32 -- corresponednt labels for train and validation splits
  """
  x_train, x_test, y_train, y_test = train_test_split(
      features, labels, test_size=test_size, shuffle=shuffle)
  print("training examples: %d", len(y_train))
  print("evaluation examples: %d", len(y_test))
  return x_train, x_test, y_train, y_test


def xgb_train_estimator(data_train,
                        data_test,
                        save_model_location,
                        num_rounds,
                        num_rounds_early_stopping,
                        num_classes,
                        use_gpu=True,
                        use_prob=True):
  """aggregates methods to train xgb estimator

  Arguments:
      data_train {tuple} -- features and correspondent labels for train
      data_test {tuple} -- features and correspondent labels for test
      save_model_location {string} -- path where to save model, includs name
      num_rounds {int} -- number of trees will be created
      num_rounds_early_stopping {int} -- early stopping when takes place
      num_classes {int} -- number of classes

  Keyword Arguments:
      use_gpu {bool} -- use or not GPU (default: {True})
      use_prob {bool} -- give raw out from softmax function (default: {True})

  Returns:
      tuple -- metrics = accuracy, error, precision, recall, mean avg. precision
      XGBoostEstimator -- instantiated object of class XGBoostEstimator
  """
  x_train, y_train = data_train
  x_test, y_test = data_test

  xgb_est = XGBoostEstimator(
      save_model_location,
      num_rounds,
      num_classes,
      num_rounds_early_stopping,
      use_gpu=use_gpu,
      use_prob=use_prob)

  # class_weights = get_class_weights(y_train)
  # sample_weight = compute_sample_weight(class_weights, y_train)

  train = xgb_est.prepare_data_xgboost(x_train,
                                       y_train)  #, class_weights=sample_weight)

  test = xgb_est.prepare_data_xgboost(x_test, y_test)
  xgb_est.train_estimator(train, test)
  predictions = xgb_est.predict(test)

  if use_prob:
    predictions_probabilities = predictions
    predictions = np.argmax(predictions_probabilities, axis=1)

  accuracy, err, p, r, m_ap = compute_metrics(
      predictions.astype(np.uint16), y_test)

  return (accuracy, err, p, r, m_ap), xgb_est


def xgb_predict_unlabeled(estimator, data):
  """predicts using xgb estimator on unlabeled data

  Arguments:
      estimator {XGBoostEstimator} -- instantiation object class XGBoostEstimator
      data {float32} -- array containing unlabeled features

  Returns:
      float32 -- array of scores from softmax
      int32 -- labels associated to above pred. based on the greates score given
               by softmax
  """
  unlabeled_features = estimator.prepare_data_xgboost(data)
  prediction_probabilities = estimator.predict(unlabeled_features)
  predictions = np.argmax(prediction_probabilities, axis=1)

  return prediction_probabilities, predictions


def configure(config, exp, r):
  """customizing paths with lvl. sup. for exp., what run is it

  Arguments:
      config {dictionary} -- keys are names of the possible settings,
                             values are actually the values of the settings
      exp {float32} -- level of supervision
      r {int} -- what run is it values = {0, 1, ..., 9}

  Returns:
      dictionary -- config dictionary customized
  """

  config["save_model_location_v1"] = os.path.join(
      os.path.dirname(os.path.realpath(__file__)),
      config["save_model_location_v1"],
      str(exp) + "-supervision-v1-vgg.xgb")
  config["save_model_location_v2"] = os.path.join(
      os.path.dirname(os.path.realpath(__file__)),
      config["save_model_location_v2"],
      str(exp) + "-supervision-v2-revnet.xgb")
  config["save_model_location_final"] = os.path.join(
      os.path.dirname(os.path.realpath(__file__)),
      config["save_model_location_final"],
      str(exp) + "_final_estimator_revnet_vgg_ssl.xgb")
  config["history_training"] = os.path.join(
      os.path.dirname(os.path.realpath(__file__)), config["history_training"],
      str(exp) + "-co-training-exchange-metrics-ssl.txt")
  config["pickle_resulst"] = os.path.join(
      os.path.dirname(os.path.realpath(__file__)), config["pickle_resulst"],
      str(r) + "-" + str(exp) + "-co-training-exchange-metrics-ssl.pkl")

  return config


def restore_config(config):
  """ reverting changes done on the config file

  Arguments:
      config {dictionary} -- keys are names of the possible settings,
                             values are actually the values of the settings

  Returns:
      dictionary -- original config dict
  """
  config["softmax_probability"] = True
  config["save_model_location_v1"], _ = os.path.split(
      config["save_model_location_v1"])
  config["save_model_location_v2"], _ = os.path.split(
      config["save_model_location_v2"])
  config["save_model_location_final"], _ = os.path.split(
      config["save_model_location_final"])
  config["history_training"], _ = os.path.split(config["history_training"])
  config["pickle_resulst"], _ = os.path.split(config["pickle_resulst"])

  return config


def co_training_indexes(view_1, view_2, exp, config, logging, use_gpu=False):
  """aggregates everythoing need it to do co-training

  Arguments:
      view_1 {View} -- object of type view
      view_2 {View} -- object of type view
      exp {float32} -- level of supervision
      config {dictionary} -- keys are names of the possible settings,
                             values are actually the values of the settings
      logging {logging} -- instance of the logging class

  Keyword Arguments:
      use_gpu {bool} -- use GPU or not (default: {False})
  """

  save_model_location_v1 = os.path.join(
      os.path.dirname(os.path.realpath(__file__)),
      config["save_model_location_v1"])
  save_model_location_v2 = os.path.join(
      os.path.dirname(os.path.realpath(__file__)),
      config["save_model_location_v2"])
  save_model_location_final = os.path.join(
      os.path.dirname(os.path.realpath(__file__)),
      config["save_model_location_final"])
  save_txt_results = os.path.join(
      os.path.dirname(os.path.realpath(__file__)), config["history_training"])
  save_pkl_results = os.path.join(
      os.path.dirname(os.path.realpath(__file__)), config["pickle_resulst"])

  iteration = 0
  results_dict = {}
  cntrl_view = ControllerView()

  # concatenating test set
  # transductive case
  concat_features_test, concat_labels_test = cntrl_view.concatenate_views(
      view_1.train_features[view_1.ids_unlabeled],
      view_2.train_features[view_2.ids_unlabeled],
      view_1.train_labels[view_1.ids_unlabeled])

  # co-training loop
  while view_1.ids_unlabeled.size != 0 and iteration < config[
      "num_co_training_rounds"]:

    # shuffle on indexes & lbls
    result_ids_v1 = np.append(view_1.ids_labeled, view_1.ids_picked)
    result_lbls_v1 = np.append(view_1.train_labels[view_1.ids_labeled],
                               view_1.labels_picked)
    shuffle_unison(result_ids_v1, result_lbls_v1)

    logging.info("Starting training estimator view 1")

    metrics_v1, xgb_est_v1 = xgb_train_estimator(
        (view_1.get_features_by_ids(result_ids_v1), result_lbls_v1),
        (view_1.train_features[view_1.ids_unlabeled],
         view_1.train_labels[view_1.ids_unlabeled]),
        save_model_location_v1,
        config["num_rounds"],
        config["num_rounds_early_stopping"],
        config["num_classes"],
        use_gpu=use_gpu,
        use_prob=config["softmax_probability"])

    write_to_file(save_txt_results, str(metrics_v1[0]),
                  "v1-" + str(iteration) + " - acc")
    append_results_dict(results_dict, metrics_v1, exp, "v1")
    logging.info("Accuracy v1: %.3f", metrics_v1[0])

    result_ids_v2 = np.append(view_2.ids_labeled, view_2.ids_picked)
    result_lbls_v2 = np.append(view_2.train_labels[view_2.ids_labeled],
                               view_2.labels_picked)
    shuffle_unison(result_ids_v2, result_lbls_v2)

    logging.info("Starting training estimator view 2 ...")
    metrics_v2, xgb_est_v2 = xgb_train_estimator(
        (view_2.get_features_by_ids(result_ids_v2), result_lbls_v2),
        (view_2.train_features[view_2.ids_unlabeled],
         view_2.train_labels[view_2.ids_unlabeled]),
        save_model_location_v2,
        config["num_rounds"],
        config["num_rounds_early_stopping"],
        config["num_classes"],
        use_gpu=use_gpu,
        use_prob=config["softmax_probability"])

    write_to_file(save_txt_results, str(metrics_v2[0]),
                  "v2-" + str(iteration) + " - acc")
    append_results_dict(results_dict, metrics_v2, exp, "v2")
    logging.info("Accuracy v2: %.3f", metrics_v2[0])
    logging.info("Finished training both estimators")

    logging.info("Making predictions on unlabeled data")
    prediction_prob_v1, labels_v1 = xgb_predict_unlabeled(
        xgb_est_v1, view_1.get_features_by_ids(view_1.ids_unlabeled))
    prediction_prob_v2, labels_v2 = xgb_predict_unlabeled(
        xgb_est_v2, view_2.get_features_by_ids(view_2.ids_unlabeled))

    logging.info("Uncertanty computing")
    # confidance based using probabilities predicted
    scores_v1 = cntrl_view.selection_confidance_based(prediction_prob_v1)
    scores_v2 = cntrl_view.selection_confidance_based(prediction_prob_v2)

    # pick top k per class with most certain scores
    indxs_top_k_per_class_v1 = view_1.get_top_k_per_class(
        labels_v1, scores_v1, 1)  # last param is k
    indxs_top_k_per_class_v2 = view_2.get_top_k_per_class(
        labels_v2, scores_v2, 1)

    # pick indexes
    # top_k_indxs_v1 = cntrl_view.select_indxs_asc(scores_v1, 200)
    # top_k_indxs_v2 = cntrl_view.select_indxs_asc(scores_v2, 200)

    ids_v2 = np.copy(
        view_2.ids_unlabeled[indxs_top_k_per_class_v2])  # top_k_indxs_v2
    ids_v1 = np.copy(
        view_1.ids_unlabeled[indxs_top_k_per_class_v1])  # top_k_indxs_v1
    view_1.append_picked_ids(ids_v2)
    view_2.append_picked_ids(ids_v1)
    view_2.append_picked_labels(labels_v1[indxs_top_k_per_class_v1])
    view_1.append_picked_labels(labels_v2[indxs_top_k_per_class_v2])

    # ids_v2 = np.copy(view_2.ids_unlabeled[top_k_indxs_v2])  # top k 500
    # ids_v1 = np.copy(view_1.ids_unlabeled[top_k_indxs_v1])
    # view_1.append_picked_ids(ids_v2)
    # view_2.append_picked_ids(ids_v1)
    # view_2.append_picked_labels(labels_v1[top_k_indxs_v1])
    # view_1.append_picked_labels(labels_v2[top_k_indxs_v2])

    # remove indexes from unlabeled indexes list
    view_1.remove_unlabeled_ids(ids_v1)
    view_2.remove_unlabeled_ids(ids_v2)

    dict_picked_ids_labels = cntrl_view.union_picked_data(
        view_1.ids_picked, view_2.ids_picked, view_1.labels_picked,
        view_2.labels_picked)

    union_ids = np.asarray(list(dict_picked_ids_labels.keys()), dtype=np.uint64)
    union_labels = np.squeeze(
        np.asarray(list(dict_picked_ids_labels.values()), dtype=np.uint32))

    copy_union_labels = np.copy(union_labels)

    union_ids = np.append(union_ids, view_1.ids_labeled)
    union_labels = np.append(union_labels,
                             view_1.train_labels[view_1.ids_labeled])

    logging.info("Concatenating features")
    features_v1 = view_1.get_features_by_ids(union_ids)
    features_v2 = view_2.get_features_by_ids(union_ids)

    concat_features, concat_labels = cntrl_view.concatenate_views(
        features_v1, features_v2, union_labels)

    shuffle_unison(concat_features, concat_labels)

    metrics_join, _ = xgb_train_estimator(
        (concat_features, concat_labels),
        (concat_features_test, concat_labels_test),
        save_model_location_final,
        config["num_rounds"],
        config["num_rounds_early_stopping"],
        config["num_classes"],
        use_gpu=use_gpu,
        use_prob=False)  #config["num_rounds"],
    logging.info("Accuracy: %.3f", metrics_join[0])
    append_results_dict(results_dict, metrics_join, exp, "join")

    if len(np.unique(copy_union_labels)) == 257:  # 102
      missing = -1
    else:
      missing = np.setdiff1d(np.arange(0, 257), np.unique(copy_union_labels))

    write_to_file(
        save_txt_results, str(metrics_join[0]),
        str(iteration) + " - # fake_lbl " + str(len(copy_union_labels)) +
        " - missing " + str(missing) + " - acc")

    iteration = iteration + 1

  # saving results
  pickle_results(save_pkl_results, results_dict)


if __name__ == "__main__":

  # changes to using only indexes
  config = configs["co-training-cpu"]
  logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)
  features_vgg, labels_vgg = load_compressed_features(config["vgg"])
  features_resnet, labels_resnet = load_compressed_features(config["resnet"])
  concat_f = np.concatenate((features_vgg, features_resnet), axis=1)

  # splitting data concat_f
  # x_train, x_test, y_train, y_test = split_data(
  # concat_f, labels_resnet, test_size=0.1, shuffle=True)

  # shuffle for transductive
  shuffle_unison(concat_f, labels_resnet)

  if config["restore"]:
    logging.info("Restoring the experiments")
    experiment = read_checkpoint_file(config["restoring_file"])
    indx = config["levels_supervision"].index(experiment)
    # update list of experiments
    config["levels_supervision"] = config["levels_supervision"][indx:]

  for exp in config["levels_supervision"]:
    # creating distinctive identifiers for models name based on exp.
    for i in range(0, config["no_runs_experiments"]):
      config = configure(config, exp, i)
      logging.info(
          "Location for future models:\n %s \n %s" %
          (config["save_model_location_v1"], config["save_model_location_v2"]))
      logging.info(
          "Updated metrics saved here: %s" % config["history_training"])

      # writing to file experiment level supervision for restoring purposes
      write_to_file(
          os.path.join(
              os.path.dirname(os.path.realpath(__file__)),
              config["restoring_file"]), str(exp), "level_supervision: ")

      # take x% data to be supervised and the rest unlabeled
      logging.info("Picking %.2f data of supervision", exp)

      # get indexes only
      ids_labeled, ids_unlabeled = get_level_supervision_indxs(
          labels_resnet, exp)  #y_train

      ids_labeled_v1 = np.copy(ids_labeled)
      ids_unlabeled_v1 = np.copy(ids_unlabeled)
      ids_labeled_v2 = np.copy(ids_labeled)
      ids_unlabeled_v2 = np.copy(ids_unlabeled)

      view_1 = View(ids_labeled_v1, ids_unlabeled_v1, concat_f[:, :256],
                    labels_resnet)
      #features_resnet[ids_unlabeled_v1][:, :256],
      #labels_resnet[ids_unlabeled_v1])  #256
      view_2 = View(ids_labeled_v2, ids_unlabeled_v2, concat_f[:, 256:],
                    labels_resnet)
      # features_resnet[ids_unlabeled_v2][:, 256:],
      # labels_resnet[ids_unlabeled_v2])

      # co-trainig call
      logging.info("Start co-training")
      start = time.time()
      # transform lists down into objects
      co_training_indexes(
          view_1, view_2, exp, config, logging, use_gpu=config["use_gpu"])
      end = time.time()
      logging.info("End co-training %.3f", end - start)
      # writing time to file
      write_to_file(
          os.path.join(
              os.path.dirname(
                  os.path.realpath(__file__)), config["history_training"]),
          str(end - start), "time co-training: ")

      # reinitialize the config dictionary
      config = restore_config(config)
