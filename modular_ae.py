"""
modular_ae.py module contains the implementation of modular autoencoder
"""
import os
import pickle

import numpy as np
import tensorflow as tf

from six.moves import xrange
from sklearn.model_selection import train_test_split
from utils.utils import load_compressed_features, load_extracted_features


class ModularAE:
  """
  encapsulates train, predict, proccess data to train MAE
  """

  def __init__(self,
               batch_size,
               num_training_updates,
               test_size=0.1,
               dataset='caltech256'):
    self.batch_size = batch_size
    self.lmbd = 0.3
    self.num_training_updates = num_training_updates
    self.test_size = test_size
    self.sess = tf.Session()
    self.graph = tf.get_default_graph()
    self.save_summaries = os.path.join(
        os.path.dirname(os.path.realpath(__file__)), 'output', 'summaries_ae')
    self.save_ckpt = os.path.join(
        os.path.dirname(os.path.realpath(__file__)), 'output', 'ckpt_ae',
        dataset, 'modularAE-resnet-vgg-lmb-' + str(self.lmbd))
    self.data_location_vgg = os.path.join(
        os.path.dirname(
            os.path.dirname(os.path.dirname(os.path.realpath(__file__)))),
        'datasets', dataset, 'vgg')
    # dataset folder should be placed outside co-training project
    # structure: datasets -> caltech256/ caltech101 (name of dataset used) ->
    # vgg  (name of feature extractor used) -> folders named after the class
    self.data_location_resnet = os.path.join(
        os.path.dirname(
            os.path.dirname(os.path.dirname(os.path.realpath(__file__)))),
        'datasets', dataset, 'resnet')

    self.features_vgg, self.labels_vgg = load_extracted_features(
        self.data_location_vgg)  #4096 vgg # 2048 resnet
    self.features_vgg = np.squeeze(self.features_vgg, axis=1)
    self.features_resnet, self.labels_resnet = load_extracted_features(
        self.data_location_resnet)
    self.features_resnet = np.squeeze(self.features_resnet, axis=1)
    self.features = np.concatenate((self.features_vgg, self.features_resnet),
                                   axis=1)
    self.labels = self.labels_resnet

  def split_data(self, features, labels):
    """splits data into train and validation

    Arguments:
        features {float32} -- feature set
        labels {uint32} -- labels correspondent features

    Returns:
        float32 -- set feature train and labels
        float32 -- set feature validation and labels
        int -- lenght of train test
        int -- lenght of validation test
    """
    x_train, x_test, y_train, y_test = train_test_split(
        features, labels, test_size=self.test_size)

    train_data_dict = {'features': x_train, 'labels': y_train}
    valid_data_dict = {'features': x_test, 'labels': y_test}

    return train_data_dict, valid_data_dict, len(x_train), len(x_test)

  def data_iterators(self, train_data, validation_data, lenght_train_data,
                     lenght_validation_data):
    """creates data iterators which split into batches and other operations

    Arguments:
        train_data {dictionary} -- {'features': arr, 'labels': arr}
        validation_data {dictionary} -- {'features': arr, 'labels': arr}
        lenght_train_data {scalr int32} -- len train set
        lenght_validation_data {scalar int32} -- len val set

    Returns:
        iterator -- operation for train iterator
        iterator -- operation for val. iterator
    """

    train_dataset_iterator = (tf.data.Dataset.from_tensor_slices(
        train_data).shuffle(lenght_train_data).batch(
            self.batch_size)).make_initializable_iterator()
    valid_dataset_iterator = (
        tf.data.Dataset.from_tensor_slices(validation_data).repeat(1)  # 1 epoch
        .batch(self.batch_size)).make_initializable_iterator()

    self.train_dataset_batch = train_dataset_iterator.get_next()
    train_iterator_init_op = train_dataset_iterator.initializer
    self.valid_dataset_batch = valid_dataset_iterator.get_next()
    valid_iterator_init_op = valid_dataset_iterator.initializer

    return train_iterator_init_op, valid_iterator_init_op

  def get_features(self, subset='train'):
    """returns features correspondent key word train/valid

    Keyword Arguments:
        subset {str} -- key word (default: {'train'})

    Returns:
        float32 -- batch of features for train/validation
    """

    if subset == 'train':
      return self.sess.run(self.train_dataset_batch)
    elif subset == 'valid':
      return self.sess.run(self.valid_dataset_batch)

  def get_modular_ae_architecture_ensamble(self, x):
    """returns architecture for modular AE

    Arguments:
        x {float32} -- input fetaures

    Returns:
        float32 -- logits -anything before a softmax layer
    """
    net1 = tf.keras.layers.Dense(
        units=2048, activation='relu', name='fc_input1')(x)
    m1 = tf.keras.layers.Dense(
        units=256, activation='relu', name='module1')(net1)
    net1 = tf.keras.layers.Dense(
        units=6144, activation='relu', name='fc_final1')(m1)

    net2 = tf.keras.layers.Dense(
        units=2048, activation='relu', name='fc_input2')(x)  # 1024
    m2 = tf.keras.layers.Dense(
        units=256, activation='relu', name='module2')(net2)
    net2 = tf.keras.layers.Dense(
        units=6144, activation='relu', name='fc_final2')(m2)
    return net1, net2

  def train(self):
    """
      trains MAE by agrgreating methods from above
    """
    train_data_dict, valid_data_dict, len_train, len_validation = self.split_data(
        self.features, self.labels)
    train_iterator_init_op, valid_iterator_init_op = self.data_iterators(
        train_data_dict, valid_data_dict, len_train, len_validation)
    features = tf.placeholder(
        tf.float32, [None, self.features.shape[1]], name='input')  # 6144

    step_var = tf.train.create_global_step()

    logits_1, logits_2 = self.get_modular_ae_architecture_ensamble(features)
    # loss
    loss_recon = tf.math.add(
        tf.reduce_mean(tf.square(logits_1 - features)),
        tf.reduce_mean(tf.square(logits_2 - features))) / 2
    avg_logits = tf.reduce_mean(tf.math.add(logits_1, logits_2))
    loss_diversity = tf.reduce_mean(
        tf.math.add(
            tf.math.subtract(logits_1, avg_logits),
            tf.math.subtract(logits_2, avg_logits)))
    loss = tf.abs(loss_recon - self.lmbd * loss_diversity)

    # otpimizer
    optimizer = tf.train.AdamOptimizer(learning_rate=0.001, beta1=0.5)
    step = optimizer.minimize(loss, global_step=step_var)

    _ = tf.summary.scalar('loss_validation', loss)
    init = tf.global_variables_initializer()
    merged = tf.summary.merge_all()

    self.saver = tf.train.Saver(max_to_keep=1)
    summary_writer = tf.summary.FileWriter(self.save_summaries, self.sess.graph)
    summary_writer_val = tf.summary.FileWriter(
        os.path.join(self.save_summaries, 'val'), self.sess.graph)
    self.sess.run(init)

    for i in xrange(self.num_training_updates):
      self.sess.run(train_iterator_init_op)

      dict_train = self.get_features(subset='train')
      feed_dict = {features: dict_train['features']}
      results = self.sess.run([step, merged, loss], feed_dict=feed_dict)

      summary_writer.add_summary(results[1], i)
      if i % 10 == 0:
        print("Iteration: (%3d) -- Loss: (%.5f) " % (i, results[2]))

      if i % 100 == 0:
        self.sess.run(valid_iterator_init_op)
        valid_dict = self.get_features(subset='valid')
        feed_dict = {features: valid_dict['features']}
        results_val = self.sess.run([step, loss, merged], feed_dict=feed_dict)
        print("Validation Iteration: (%3d) -- Val_Loss: (%.5f)" %
              (i, results_val[1]))
        loss_val = results_val[1]

        summary_writer_val.add_summary(results_val[2], i)

        if i == 0:
          print("Initializing best loss ...")
          best_val_loss = loss_val

        if loss_val <= best_val_loss:
          best_val_loss = loss_val
          save_path = self.saver.save(
              self.sess,
              '%s/Epoch_%d.ckpt' % (self.save_ckpt, i),
              global_step=step_var)
          print('Model is saved in file: %s' % save_path)

  def predict(self, meta_file):
    """makes predictions

    Arguments:
        meta_file {string} -- name of the latest meta file

    Returns:
        float32 -- logits from encoder 1
        float32 -- logits from encoder 2
    """
    saver = tf.train.import_meta_graph(os.path.join(self.save_ckpt, meta_file))
    saver.restore(self.sess, tf.train.latest_checkpoint(self.save_ckpt))

    print("Model restored ...")
    input_features = self.graph.get_tensor_by_name('input:0')
    output_encoded_1 = self.graph.get_tensor_by_name('module1/Relu:0')
    output_encoded_2 = self.graph.get_tensor_by_name('module2/Relu:0')

    # predict
    output = self.sess.run([output_encoded_1, output_encoded_2],
                           feed_dict={input_features: self.features})
    e1, e2 = output[0], output[1]

    return e1, e2

  def save_predictions(self, enc1, enc2, folder_name, no_classes=256):
    """saves the predictions made

    Arguments:
        enc1 {float32} -- array with encodings made by encoder 1 from MAE
        enc2 {float32} -- array with encodings made by encoder 2 from MAE
        folder_name {string} -- directory name where to save predictions
    """
    to_save = os.path.join(
        os.path.dirname(self.data_location_vgg), folder_name, 'modular')
    concat_encodings = np.concatenate((enc1, enc2), axis=1)
    for i in range(0, no_classes):
      indxs_class = np.where(self.labels == i)[0]
      f = open(os.path.join(to_save, '%s.modular' % i), 'wb')

      pickle.dump(concat_encodings[indxs_class], f)
      f.close()


if __name__ == "__main__":
  mod_ae = ModularAE(32, 9000, test_size=0.1)  #9200
  mod_ae.train()
  enc1, enc2 = mod_ae.predict('Epoch_8300.ckpt-8385.meta')
  #mod_ae.save_predictions(enc1, enc2, 'vgg_resnet_modular', no_classes=256)
  # if doing for caltech 102 must change default value no_classes ^
