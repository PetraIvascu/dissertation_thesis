"""
ui_results_app.py module creates graphical interface using DASH and Plotly
"""
import os
import glob
import pickle
import dash
from dash.dependencies import Input, Output
import dash_daq as daq
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go

from utils.utils_plot import get_acc_level_supervison, get_acc
from utils.utils_plot import get_map_level_supervison
from utils.utils_plot import get_pseudo_labes_add, get_std_level_supervison


methods_exp = [
    "MAE-Resnet", "MAE-VGG", "MAE-VGG-Resnet", "Resnet-VGG", "XGB estimator"
]
datasets = ['Caltech 101', 'Caltech 256']

def get_data_comparison(dataset='c256'):
  """returns data for comparison

  Keyword Arguments:
      dataset {str} -- dataset options (default: {'c256'})

  Returns:
      list -- string
  """
  if dataset == 'c256':
    proc_results = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                'output',
                                'results_processed',
                                'caltech_256',
                                'comparison')
  elif dataset == 'c101':
    proc_results = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                'output',
                                'results_processed',
                                'caltech_101',
                                'comparison')

  files = glob.glob("{}\*.pkl".format(proc_results))
  all_data_files = []
  for f in files:
    with open(f, 'rb') as pklf:
      res = pickle.load(pklf)
    all_data_files.append(res)

  return all_data_files


def get_data(dataset='c256'):
  """returns data corresponednt to dataset

  Keyword Arguments:
      dataset {str} -- dataset options (default: {'c256'})

  Returns:
      list -- string
  """
  if dataset == 'c256':
    proc_results = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                'output',
                                'results_processed',
                                'caltech_256')
  else:
    proc_results = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                'output',
                                'results_processed',
                                'caltech_101')

  files = glob.glob("{}\*.pkl".format(proc_results))
  all_data_files = []
  for f in files:
    with open(f, 'rb') as pklf:
      res = pickle.load(pklf)
    all_data_files.append(res)

  return all_data_files


def prepare_layout(title=None,
                   xaxis_title='supervision',
                   yaxis_title='accuracy'):
  """prepares layout

  Keyword Arguments:
      title {string} -- title figure (default: {None})
      xaxis_title {str} -- name for axis (default: {'supervision'})
      yaxis_title {str} -- name for axis (default: {'accuracy'})

  Returns:
      dicitonary -- dict with appropriate format
  """

  new_layout_format = {
      'layout': title,
      'yaxis': dict(title=yaxis_title),
      'xaxis': dict(title=xaxis_title)
  }

  return new_layout_format


def prepare_data(data,
                 fake_labeled=None,
                 value_lvl_sup=0,
                 supervision=None,
                 error_y=None,
                 use_map=False,
                 type_plot='scatter',
                 mode='lines+markers',
                 name_methods=methods_exp):
  """prepares data for grapg component

  Arguments:
      data {tuple} -- tuple containing various metrics

  Keyword Arguments:
      fake_labeled {list} -- given or not (default: {None})
      value_lvl_sup {int} -- #instances per class (default: {0})
      supervision {list} -- given or not (default: {None})
      error_y {list} -- was given or not (default: {None})
      use_map {bool} -- if plot map or not (default: {False})
      type_plot {str} -- type of the graph (default: {'scatter'})
      mode {str} -- type of graph (default: {'lines+markers'})
      name_methods {str} -- name of the methods plotted (default: {methods_exp})

  Returns:
      dict -- plot components
  """

  new_data_format = []
  color = ['#b56760', '#99ccff', '#666699', '#669999', '#b6bf84']

  if error_y is None:
    if supervision is not None and use_map is False:
      # means -> plot supervision agianst acc
      for i, method in enumerate(name_methods): #range(0, len(data[1])):
        plot_compo = {
            'x': supervision,
            'y': data[i],
            'type': type_plot,
            'mode': mode,
            'line': dict(color=color[i]),
            'name': method
        }
        new_data_format.append(plot_compo)

    if supervision is not None and use_map is True:
      # means -> plot supervision agianst map
      for i, method in enumerate(name_methods):
        plot_compo = {
            'x': supervision,
            'y': data[i],
            'type': type_plot,
            'mode': mode,
            'line': dict(color=color[i]),
            'name': method
        }
        new_data_format.append(plot_compo)

    if fake_labeled is not None:
      # means -> plot fake labeled against acc fake per lvl sup
      m = name_methods[:4]
      for i in range(0, len(m)):
        plot_compo = {
            'x': fake_labeled[i][value_lvl_sup],
            'y': data[i][value_lvl_sup],
            'type': type_plot,
            'mode': mode,
            'line': dict(color=color[i]),
            'name': m[i]
        }
        new_data_format.append(plot_compo)

  if error_y is not None:
    for i, method in enumerate(name_methods):
      plot_compo = {
          'x': supervision,
          'y': data[i],
          'error_y': dict(type='data', array=error_y[i], visible=True),
          'type': type_plot,
          'line': dict(color=color[i]),
          'name': method
      }
      new_data_format.append(plot_compo)

  return new_data_format


def dash_app():
  """creates the laypout

  Returns:
      Dash -- returns app
  """
  external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
  app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

  tab_style = {
      'borderBottom': '1px solid #d6d6d6',
      'padding': '6px',
      'fontWeight': 'bold'
  }

  tab_selected_style = {
      'borderTop': '1px solid #d6d6d6',
      'borderBottom': '1px solid #d6d6d6',
      'backgroundColor': '#119DFF',
      'color': 'white',
      'padding': '6px'
  }

  app.layout = html.Div([
      html.Div([
          html.Div([
              dcc.Dropdown(
                  id='dataset-dropdown',
                  clearable=False,
                  options=[
                      {'label': i, 'value': i} for i in datasets
                  ], placeholder="Select dataset"
                  )], style={'marginTop': 30, 'marginBottom': 30},
                   className='six columns')

      ], className='row'),
      dcc.Tabs(
          id='tabs',
          children=[
              dcc.Tab(label='Metrics', children=[
                  html.Div([
                      html.Div([
                          dcc.RadioItems(id='data-view', options=[
                              {'label': 'Scatter', 'value': 'Scatter'},
                              {'label': 'Error', 'value': 'Error'},],
                                         value='Scatter',
                                         labelStyle={'display': 'inline-block'})
                      ], style={'marginTop': 30, 'marginLeft': 30},
                               className='six columns')

                  ], className='row'),

                  html.Div([
                      html.Div(dcc.Graph(id='error'),
                               style={'height': '800',
                                      'display': 'inline-block'},
                               className='six columns'),

                      html.Div(
                          dcc.Graph(id='map',
                                    figure={
                                        'data': prepare_data((supervision, map_exp_c256)),
                                        'layout': prepare_layout(yaxis_title='map')}),
                          style={'height': '800', 'display': 'inline-block'},
                          className='six columns'
                      ),
                  ], className='row')
              ], style=tab_style, selected_style=tab_selected_style),
              dcc.Tab(label='Pseudo Labels', children=[
                  html.Div([
                      html.Div([
                          dcc.Graph(id='fake_vs_acc')
                      ],
                               style={'height': '80', 'display': 'inline-block',
                                      'marginLeft': 150},
                               className='seven columns'),

                      html.Div([
                          daq.Slider(id='slider', vertical=True,
                                     min=2, max=20, value=2, step=None,
                                     marks={'2': '2', '3': '3', '5': '5',
                                            '10': '10', '15': '15', '20': '20'})
                      ], style={'marginTop': 100}, className='two columns')
                  ], className='row'),
              ], style=tab_style, selected_style=tab_selected_style),
              dcc.Tab(label='Comparison', children=[
                  html.Div([
                      dcc.Graph(id='compare')
                  ], style={'height': '80', 'display': 'inline-block',
                            'marginLeft': 150}, className='seven columns')
              ], style=tab_style, selected_style=tab_selected_style),
          ])
  ])

  return app


# data need
supervision = [2, 3, 5, 10, 15, 20]
data_all_c256 = get_data(dataset='c256')
data_all_c101 = get_data(dataset='c101')

acc_exp_c256 = get_acc(data_all_c256)
map_exp_c256 = get_map_level_supervison(data_all_c256)
std_exp_c256 = get_std_level_supervison(data_all_c256)
acc_fake_c256 = get_acc_level_supervison(data_all_c256)
fake_labeled_c256 = get_pseudo_labes_add(data_all_c256)

to_compare_c256 = [0.295, 0.325, 0.350, 0.365, 0.37, 0.38, 0.395, 0.4, 0.41,
                   0.42]
best_mine_c256 = get_data_comparison(dataset='c256')
acc_best_mine_c256 = get_acc(best_mine_c256)
comparison_c256 = [acc_best_mine_c256[0], to_compare_c256]
methods_comparison_c256 = ["MAE-Resnet", "McPMVC"]
supervision_comparison_c256 = [5, 10, 15, 20, 25, 30, 35, 40, 45, 50]

acc_exp_c101 = get_acc(data_all_c101)
map_exp_c101 = get_map_level_supervison(data_all_c101)
std_exp_c101 = get_std_level_supervison(data_all_c101)
acc_fake_c101 = get_acc_level_supervison(data_all_c101)
fake_labeled_c101 = get_pseudo_labes_add(data_all_c101)

to_compare_c101 = [0.725, 0.78, 0.81, 0.84, 0.849, 0.85]
best_mine_c101 = get_data_comparison(dataset='c101')
map_best_mine_c101 = get_map_level_supervison(best_mine_c101)
comparison_c101 = [map_best_mine_c101[0], to_compare_c101]
methods_comparison_c101 = ["VGG-Resnet", "CURL"]

app = dash_app()


@app.callback(
    [
        Output('error', 'figure'),
        Output('map', 'figure'),
        Output('fake_vs_acc', 'figure'),
        Output('compare', 'figure')
    ],
    [
        Input('data-view', 'value'),
        Input('dataset-dropdown', 'value'),
        Input('slider', 'value')
    ])
def update_fig(dataview, dset_dropdown, slider_value):
  """method used by callback above

  Arguments:
      dataview {str} -- result from dataview component
      dset_dropdown {str} -- value from dropdown
      slider_value {str} -- value from slider

  Returns:
      dictionary -- figures to update divs froma app
  """

  if dset_dropdown == 'Caltech 101':
    data = (supervision, acc_exp_c101, std_exp_c101, map_exp_c101,
            acc_fake_c101,
            fake_labeled_c101) if dataview == 'Error' else (supervision,
                                                            acc_exp_c101, None,
                                                            map_exp_c101,
                                                            acc_fake_c101[:4],
                                                            fake_labeled_c101)
    data_compare = comparison_c101
    supervision_compare = supervision
    methods_compare = methods_comparison_c101
    use_map = True
  else:
    data = (supervision, acc_exp_c256, std_exp_c256, map_exp_c256,
            acc_fake_c256,
            fake_labeled_c256) if dataview == 'Error' else (supervision,
                                                            acc_exp_c256, None,
                                                            map_exp_c256,
                                                            acc_fake_c256[:4],
                                                            fake_labeled_c256)
    data_compare = comparison_c256
    supervision_compare = supervision_comparison_c256
    methods_compare = methods_comparison_c256
    use_map = False

  if data[2] is not None:
    figure = {
        'data':
        prepare_data(data[1], supervision=data[0], error_y=data[2]),
        'layout':
        go.Layout(
            yaxis=dict(title='Accuracy'), xaxis=dict(title='Number of labeled images per class'))
    }

  else:

    figure = {
        'data':
        prepare_data(data[1], supervision=data[0]),
        'layout':
        go.Layout(
            yaxis=dict(title='Accuracy'), xaxis=dict(title='Number of labeled images per class'))
    }

  map_fig = {
      'data': prepare_data(data[3], supervision=data[0], use_map=True),
      'layout': go.Layout(
          yaxis=dict(title='MAP'), xaxis=dict(title='Number of labeled images per class'))
  }

  fake_vs_acc_fig = {
      'data':
      prepare_data(
          data[4],
          fake_labeled=data[5],
          value_lvl_sup=data[0].index(int(slider_value))),
      'layout':
      go.Layout(yaxis=dict(title='Accuracy'), xaxis=dict(title='Number of pseudo-labels'))
  }

  comparison = {
      'data': prepare_data(data_compare,
                           supervision=supervision_compare,
                           use_map=use_map,
                           name_methods=methods_compare),
      'layout': go.Layout(yaxis=dict(title='MAP' if use_map else 'Accuracy'),
                          xaxis=dict(title='Number of labeled images per class'))
  }

  return figure, map_fig, fake_vs_acc_fig, comparison


if __name__ == '__main__':
  app.run_server(debug=True)
