"""
supervised_estimator.py trains simple xbg estimator
"""
import sys
import logging
import os
import numpy as np
from sklearn.model_selection import train_test_split
from split_level_supervision import get_level_supervision_indxs
from utils import write_to_file, read_checkpoint_file
from utils import compute_metrics, pickle_results
from utils import load_compressed_features
from xgboost_estimator import XGBoostEstimator
where, _ = os.path.split(os.path.realpath(__file__))
sys.path.append(where)
from configs import configs


def append_results_dict(result_dict, metrics, supervision):
  """adds given results to a dictionary

  Arguments:
      result_dict {dictionary} -- dictionary supervision levels and acc values
      metrics {float32} -- array containg various metrics
      supervision {float32} -- level of supervision training on
  """

  if len(result_dict) == 0:
    result_dict['id'] = supervision
    result_dict['acc'] = [metrics[0]]
    result_dict['err'] = [metrics[1]]
    result_dict['p'] = [metrics[2]]
    result_dict['r'] = [metrics[3]]
    result_dict['m_ap'] = [metrics[4]]
  else:
    result_dict['acc'].append(metrics[0])
    result_dict['err'].append(metrics[1])
    result_dict['p'].append(metrics[2])
    result_dict['r'].append(metrics[3])
    result_dict['m_ap'].append(metrics[4])


def split_data(features, labels, test_size, shuffle=True):
  """splits data into train and test protion

  Arguments:
      features {float32} -- set of features
      labels {uint32} -- labesl correspondent features
      test_size {float32} -- test size float

  Keyword Arguments:
      shuffle {bool} -- shuffles data or not (default: {True})

  Returns:
      float32 -- features for test, train
      uint32 -- labels for test, train
  """
  x_train, x_test, y_train, y_test = train_test_split(
      features, labels, test_size=test_size, shuffle=shuffle)

  return x_train, x_test, y_train, y_test


def train_estimator(data,
                    save_model_location,
                    num_rounds,
                    num_rounds_early_stopping,
                    num_classes,
                    use_gpu=True,
                    use_prob=True):
  """trains xgb fully supervised

                    Arguments:
                        data {tuple} -- contains test and train data
                        save_model_location {string} -- where to save model
                        num_rounds {int} -- number of trees for xgb
                        num_rounds_early_stopping {int} -- number of rounds till
                        early stop
                        num_classes {int} -- number of classes

                    Keyword Arguments:
                        use_gpu {bool} -- if train ong gpu or not (default: {True})
                        use_prob {bool} -- use probabilities or not (default: {True})
  """

  x_train, x_test, y_train, y_test = data[0], data[1], data[2], data[3]
  xgb_est = XGBoostEstimator(
      save_model_location,
      num_rounds,
      num_classes,
      num_rounds_early_stopping,
      use_gpu=use_gpu,
      use_prob=use_prob)

  train = xgb_est.prepare_data_xgboost(x_train, y_train)
  test = xgb_est.prepare_data_xgboost(x_test, y_test)
  xgb_est.train_estimator(train, test)
  predictions = xgb_est.predict(test)
  acc, error, p, r, multiclass_average_precision = compute_metrics(
      predictions, y_test)

  return (acc, error, p, r, multiclass_average_precision), xgb_est


def configure(config, exp, r):
  """configures paths from config file

  Arguments:
      config {dictionary} -- keys give a setting
      exp {float32} -- level od supervision
      r {int} -- how far got till redoing experiments eg 2 of 10

  Returns:
      dictionary -- dictionary with prepared settings
  """
  config["save_model_location"] = os.path.join(
      os.path.dirname(os.path.realpath(__file__)),
      config["save_model_location"],
      str(exp) + "_estimator_revnet_vgg_sl.xgb")
  config["history_training"] = os.path.join(
      os.path.dirname(os.path.realpath(__file__)), config["history_training"],
      str(exp) + "-metrics-sl.txt")
  config["pickle_result"] = os.path.join(
      os.path.dirname(os.path.realpath(__file__)), config["pickle_result"],
      str(r) + "-" + str(exp) + "-estimator-metrics-sl.pkl")

  return config


def restore_config(config):
  config["save_model_location"], _ = os.path.split(
      config["save_model_location"])
  config["history_training"], _ = os.path.split(config["history_training"])
  config["pickle_result"], _ = os.path.split(config["pickle_result"])

  return config


if __name__ == "__main__":

  config = configs["supervised_estimator-cpu"]
  logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)
  features_vgg, labels_vgg = load_compressed_features(config["vgg"])
  features_resnet, labels_resnet = load_compressed_features(config["resnet"])

  concat_f = np.concatenate((features_vgg, features_resnet), axis=1)

  # splitting data
  x_train, x_test, y_train, y_test = split_data(
      concat_f, labels_vgg, test_size=0.1, shuffle=True)

  if config["restore"]:
    logging.info("Restoring the experiments")
    experiment = read_checkpoint_file(config["restoring_file"])
    indx = config["levels_supervision"].index(experiment)
    # update list of experiments
    config["levels_supervision"] = config["levels_supervision"][indx:]

  logging.info("Preparing to start experiments ...")

  for exp in config["levels_supervision"]:
    for i in range(0, 10):  # how many times repeat experiment
      result_dict = {}
      # creating distinctive identifiers for models name based on exp.
      config = configure(config, exp, i)
      save_pkl_results = os.path.join(
          os.path.dirname(os.path.realpath(__file__)), config["pickle_result"])
      logging.info(
          "Location where model is saved: %s" % config["save_model_location"])
      logging.info(
          "Updated metrics saved here: %s" % config["history_training"])
      # writing to file experiment level supervision for restoring purposes
      write_to_file(
          os.path.join(
              os.path.dirname(os.path.realpath(__file__)),
              config["restoring_file"]), str(exp), "level_supervision: ")

      # take x% data to be supervised and the rest unlabeled
      logging.info("Picking %.3f data of supervision", exp)
      ids_labeled, ids_unlabeled = get_level_supervision_indxs(y_train, exp)

      # call estimator for training
      metrics, xgb_est = train_estimator(
          (x_train[ids_labeled], x_test, y_train[ids_labeled], y_test),
          config["save_model_location"], config["num_rounds"],
          config["num_rounds_early_stopping"], config["num_classes"],
          config["use_gpu"], config["softmax_probability"])

      logging.info("Accuracy: %.3f", metrics[0])
      write_to_file(config["history_training"], str(metrics[0]), "acc - ")
      append_results_dict(result_dict, metrics, exp)
      pickle_results(save_pkl_results, result_dict)
      # reinitialize the config dictionary
      config = restore_config(config)
