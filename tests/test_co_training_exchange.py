"""
test_co-training+exchange.py module tests
"""
import sys
import unittest
import numpy as np

sys.path.insert(
    0, 'C:\\Users\\ivasc\\Documents\\disertatie\\projects\\co-training\\')

from controller_view import ControllerView
from split_level_supervision import get_level_supervision_indxs


class GetLevelSupervisionIndxsTest(unittest.TestCase):

  def __init__(self, *args, **kwargs):
    super(GetLevelSupervisionIndxsTest, self).__init__(*args, **kwargs)
    self.features_view_1 = np.random.rand(100, 4096)
    self.labels_view_1 = np.random.randint(
        low=0, high=10, size=(100,), dtype=np.uint16)
    self.precentage_supervision = 0.3
    self.dict_lbl_v1, self.dict_unlbl_v1 = get_level_supervision_indxs(
        self.labels_view_1, self.precentage_supervision)

  def test_type_features(self):
    self.assertEqual(self.features_view_1.dtype, np.float64)

  def test_type_labels(self):
    self.assertEqual(self.labels_view_1.dtype, np.uint16)

  def test_all_labels_representatives_labeled_data(self):
    all_unique_labels = np.unique(self.labels_view_1)
    bool_list = []
    for i, _ in enumerate(self.dict_lbl_v1):
      bool_list.append(i in all_unique_labels)
    self.assertFalse(not any(bool_list), False)

  def test_all_labels_representatives_unlabeled_data(self):
    all_unique_labels = np.unique(self.labels_view_1)
    bool_list = []
    for i, _ in enumerate(self.dict_unlbl_v1):
      bool_list.append(i in all_unique_labels)
    self.assertFalse(not any(bool_list), False)

  def test_indxs_labeled_unlabeled_data_different(self):
    lbl_indxs = []
    unlbl_indxs = []

    for _, value in enumerate(list(self.dict_unlbl_v1)):
      lbl_indxs.append(value)

    for _, value in enumerate(list(self.dict_lbl_v1)):
      unlbl_indxs.append(value)

    self.assertEqual(len(set(lbl_indxs).intersection(set(unlbl_indxs))), 0)


class UnionPickedDataTest(unittest.TestCase):

  def __init__(self, *args, **kwargs):
    super(UnionPickedDataTest, self).__init__(*args, **kwargs)
    self.cntrl_view = ControllerView()

    # case 1
    # conflicts where labels differ
    self.indxs_picked_v1 = np.array([22, 0, 2, 100, 9, 19, 11], dtype=np.uint64)
    self.lbls_picked_v1 = np.array([0, 14, 45, 15, 1, 0, 0], dtype=np.uint16)
    self.indxs_picked_v2 = np.array([1, 10, 2, 22, 101, 13, 31],
                                    dtype=np.uint64)
    self.lbls_picked_v2 = np.array([0, 14, 15, 3, 1, 0, 4], dtype=np.uint16)

    # case 2
    # conflicts where labels same
    self.indxs_picked_v1_same = np.array([22, 0, 2, 100, 9, 19, 11],
                                         dtype=np.uint64)
    self.lbls_picked_v1_same = np.array([0, 14, 45, 15, 1, 0, 0],
                                        dtype=np.uint16)
    self.indxs_picked_v2_same = np.array([1, 10, 2, 22, 101, 13, 31],
                                         dtype=np.uint64)
    self.lbls_picked_v2_same = np.array([0, 14, 15, 0, 1, 0, 4],
                                        dtype=np.uint16)

    # case 3
    # conflicts where nothing is valid
    self.indxs_picked_v1_not_valid = np.array([22], dtype=np.uint64)
    self.lbls_picked_v1_not_valid = np.array([0], dtype=np.uint16)
    self.indxs_picked_v2_not_valid = np.array([22], dtype=np.uint64)
    self.lbls_picked_v2_not_valid = np.array([4], dtype=np.uint16)

  def test_conflic_labels(self):
    dict_picked_ids_labels = self.cntrl_view.union_picked_data(
        self.indxs_picked_v1, self.indxs_picked_v2, self.lbls_picked_v1,
        self.lbls_picked_v2)
    keys_dict = dict_picked_ids_labels.keys()
    self.assertEqual(bool(22 in keys_dict), bool(False))

  def test_indxs_labels_same(self):
    dict_picked_ids_labels = self.cntrl_view.union_picked_data(
        self.indxs_picked_v1_same, self.indxs_picked_v2_same,
        self.lbls_picked_v1_same, self.lbls_picked_v2_same)
    value_dict = dict_picked_ids_labels[22]

    self.assertEqual(value_dict.size, 1)

  def test_nothing_valid(self):
    dict_picked_ids_labels = self.cntrl_view.union_picked_data(
        self.indxs_picked_v1_not_valid, self.indxs_picked_v2_not_valid,
        self.lbls_picked_v1_not_valid, self.lbls_picked_v2_not_valid)
    self.assertTrue((not dict_picked_ids_labels), True)


class ConcatenateViewsTest(unittest.TestCase):

  def __init__(self, *args, **kwargs):
    super(ConcatenateViewsTest, self).__init__(*args, **kwargs)
    self.cntrl_view = ControllerView()
    self.features_view_1 = np.random.rand(100, 4096)
    self.features_view_2 = np.random.rand(100, 8192)
    self.labels_view_1 = np.random.randint(
        low=0, high=10, size=(100,), dtype=np.uint16)
    self.features_concat_result, self.labels_concat_result = self.cntrl_view.concatenate_views(
        self.features_view_1, self.features_view_2, self.labels_view_1)

  def test_count_concat_result(self):
    self.assertEqual(self.features_concat_result.shape[0],
                     self.features_view_1.shape[0])

  def test_shape_concat_result(self):
    self.assertEqual(
        self.features_concat_result.shape[1],
        self.features_view_1.shape[1] + self.features_view_2.shape[1])


class SelectIndxsAscTest(unittest.TestCase):

  def __init__(self, *args, **kwargs):
    super(SelectIndxsAscTest, self).__init__(*args, **kwargs)
    self.cntrl_view = ControllerView()
    self.scores_uncertanty = np.random.uniform(low=0.0, high=1.0, size=(200,))
    self.top_k = 10
    self.indxs_picked = self.cntrl_view.select_indxs_asc(
        self.scores_uncertanty, self.top_k)

  def test_shape_indxs(self):
    self.assertEqual(self.indxs_picked.size, self.top_k)

  def test_type_indxs(self):
    self.assertEqual(self.indxs_picked.dtype, np.uint64)


if __name__ == '__main__':
  unittest.main()
