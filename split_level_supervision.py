"""
split_level_supervision.py given data splits accordingly to the level of
supervison mentioned
"""

import math
import copy
import numpy as np


def find_frequency_classes(labels):
  """finds for how many instances each class has

  Arguments:
      labels {int32} -- array of labels

  Returns:
      tuple -- (label, amount)
  """
  y = np.bincount(labels)
  ii = np.nonzero(y)[0]

  return zip(ii, y[ii])


def compute_proc_supervision(labels, stat_classes, proc_sup=0.1):
  """determines how much means a procentage per each class

  Arguments:
      labels {int32} -- labels for features
      stat_classes {tuple} -- statistic for each (label, amount)

  Keyword Arguments:
      proc_sup {float} -- level of supervision (default: {0.1})

  Returns:
      dictionary -- each unique label has assigned how many labels should have
  """

  dict_supervision = {}
  for clss, total_entries in stat_classes:
    dict_supervision[clss] = math.ceil(proc_sup * total_entries)
  return dict_supervision


def precentage_supervision_into_number_items(labels, proc_sup):
  """finds out how many items per class will use based on total number of features

  Arguments:
      labels {int32} -- labels for features
      proc_sup {float32} -- level of supervision

  Returns:
      int -- number of items correspondent level of supervision per class
  """
  no_items = math.ceil(proc_sup * len(labels))
  no_classes = labels[np.argmax(labels)] + 1
  no_items_per_class = int(no_items / no_classes)
  if no_items_per_class < 1:
    no_items_per_class = 1

  return no_items_per_class


def split_indxs_numeric(labels, stat_classes, items_per_class):
  """Splits indexes per class in labeled and unlabeled

  Arguments:
      labels {uint32} -- labels for sets of features
      stat_classes {tuple} -- (label, amount)
      items_per_class {int} -- number of items per class proportional to
      supervision levels

  Returns:
      dictionary -- each unique label has assigned a list of indexes meaining
      those will labeled instances
      dictionary -- each unique label has assigned a list of indexes meaining
      those will unlabeled instances
  """
  picked_samples = np.array([], dtype=np.uint64)
  unpicked_samples = np.array([], dtype=np.uint64)

  for clss, _ in stat_classes:
    indxs_class = np.where(labels == clss)[0]
    picked = np.random.choice(indxs_class, items_per_class, replace=False)
    others = np.setdiff1d(indxs_class, picked)
    picked_samples = np.append(picked_samples, picked)
    unpicked_samples = np.append(unpicked_samples, others)

  return picked_samples.astype(dtype=np.uint64), unpicked_samples.astype(
      dtype=np.uint64)


def split_indxs_precentage(labels, stat_classes, supervision):
  """Splits indexes per class in labeled and unlabeled

  Arguments:
      labels {uint32} -- labels for sets of features
      stat_classes {tuple} -- (label, amount)
      supervision {dicitonary} -- {cls:amount items ...}
  }
  Returns:
      dictionary -- each unique label has assigned a list of indexes meaining
      those will labeled instances
      dictionary -- each unique label has assigned a list of indexes meaining
      those will unlabeled instances
  """
  picked_samples = np.array([], dtype=np.uint64)
  unpicked_samples = np.array([], dtype=np.uint64)

  for clss, _ in stat_classes:
    indxs_class = np.where(labels == clss)[0]
    picked = np.random.choice(indxs_class, supervision[clss], replace=False)
    others = np.setdiff1d(indxs_class, picked)
    np.append(picked_samples, picked)
    np.append(unpicked_samples, others)

  return picked_samples, unpicked_samples


def get_level_supervision_indxs(labels, proc_sup=0.1):
  """aggregades multple functions to return only labeled and unlabeld pool

  Arguments:
      labels {uint32} -- labels for sets of features

  Keyword Arguments:
      proc_sup {float32} -- level of supervision (default: {0.1})

  Returns:
      dictionary -- each unique label has assigned a list of indexes meaining
      those will labeled instances
      dictionary -- each unique label has assigned a list of indexes meaining
      those will unlabeled instances
  """
  # adding a fix number of data points to each class
  statistics_classes = find_frequency_classes(labels)
  supervision = precentage_supervision_into_number_items(labels, proc_sup)
  labeled, unlabeled = split_indxs_numeric(labels,
                                           copy.deepcopy(statistics_classes),
                                           supervision)
  # proportional with amount of data distribution per class
  # supervision = compute_proc_supervision(labels,
  #                                        copy.deepcopy(statistics_classes),
  #                                        proc_sup)
  # labeled, unlabeled = split_indxs_precentage(labels, statistics_classes,
  #                                             supervision)

  return labeled, unlabeled
