"""
xgboost_estimator.py provides basic operations for an estimator of type xgboost
"""

import xgboost as xgb
import numpy as np

from sklearn.model_selection import train_test_split


class XGBoostEstimator:
  """
  responsible with preparing data for xgb, trianing on them and other operations
  """

  def __init__(self,
               save_model_location,
               num_rounds,
               num_classes,
               early_stopping_rounds,
               use_gpu=True,
               use_prob=True):

    self.save_model_location = save_model_location
    self.num_rounds = num_rounds
    self.num_classes = num_classes
    self.early_stopping_rounds = early_stopping_rounds
    self.use_gpu = use_gpu
    self.prob = use_prob
    self.model = None

  def split_data(self, features, labels, test_size):
    """provides means of splitting data in train and test

    Arguments:
        features {float32} -- array of features
        labels {uint32} -- labels for features
        test_size {float32} -- test size as float

    Returns:
        float32 -- features for train, features for test
        uint32 -- labels for train labels for test
    """
    x_train, x_test, y_train, y_test = train_test_split(
        features, labels, test_size=test_size)

    return x_train, x_test, y_train, y_test

  def prepare_data_xgboost(self, x, y=None, class_weights=None):
    """converts data to xgboost format - helps with speed

    Arguments:
        x {float32} -- set of features

    Keyword Arguments:
        y {uint32} -- labels for provided features (default: None)
        class_weights {float32} -- class weights (default: None)

    Returns:
        DMatrix -- matrixes with data
    """
    return xgb.DMatrix(np.matrix(x), label=y, weight=class_weights)

  def train_estimator(self, Dtrain, Dtest):
    """trains using train and test data xgboost

    Arguments:
        Dtrain {Dmatrix} -- training data xgb format
        Dtest {Dmatrix} -- testing data xgb format
    """
    params = {
        'verbosity': 0,
        'subsample': 0.8,
        'max_depth': 4,
        'eta': 0.1,
        'alpha': 0.2,
        'colsample_bytree': 0.3,
        'gamma': 0.1,
        'objective': 'multi:softmax',
        'num_class': self.num_classes,
        'eval_metric': 'merror',
        'nthread': 3,
        'tree_method': 'approx'
    }

    if self.use_gpu:
      params['tree_method'] = 'gpu_hist'
    if self.prob:
      params['objective'] = 'multi:softprob'

    self.model = xgb.train(
        params,
        Dtrain,
        self.num_rounds,
        evals=[(Dtrain, "train"), (Dtest, "val")],
        verbose_eval=1,
        early_stopping_rounds=self.early_stopping_rounds)

    self.model.save_model(self.save_model_location)

  def predict(self, Dtest):
    """ predicts using trained xgb model

    Arguments:
        Dtest {DMatrix} -- testing data xgb format

    Returns:
        float32 -- array of prediction from softmax layer of xgb
    """
    predictions = self.model.predict(Dtest)
    return predictions
