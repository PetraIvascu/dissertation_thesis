"""
utils_plot.py module contains different operations useful for plotting
"""


def get_supervision_experiment(filename):
  """returns the levels of supervision for the filenames of the experiments

  Arguments:
      filename {string} -- name of an experiment

  Returns:
      string -- level of supervision for file
  """
  fname_split = filename.split("-")
  return fname_split[0]


def get_pseudo_labes_add(data):
  """Gets the number of  fake-labels added to labeled pool

  Arguments:
      data {dictionary} -- {'lvl_sup':[metrics], ...}

  Returns:
      list floats -- #pseudo-labels per round of co-training for each experiemnt
  """
  experiments_psl = []
  for d in data:
    experiment_psl = []
    for k, _ in d.items():
      experiment_psl.append(d[k][5][1])
    experiments_psl.append(experiment_psl)

  return experiments_psl


def get_acc_level_supervison(data):
  """gets the average accuracy over 10 runs - last value took in consideration

  Arguments:
      data {dictionary} --  {'lvl_sup':[metrics], ...}

  Returns:
      list floats -- avg. acc. for each lvl. of supervision
  """
  experiments_acc = []
  for d in data:
    experiment_acc = []
    for k, _ in d.items():
      experiment_acc.append(d[k][5][0])
    experiments_acc.append(experiment_acc)

  return experiments_acc


def get_map_level_supervison(data):
  """gets the average mean avg. precision over 10 runs
  last value took in consideration

  Arguments:
      data {dictionary} --  {'lvl_sup':[metrics], ...}

  Returns:
      list floats -- avg. map. for each lvl. of supervision
  """
  experiments_map = []
  for d in data:
    experiment_map = []
    for k, _ in d.items():
      experiment_map.append(d[k][0][4])
    experiments_map.append(experiment_map)

  return experiments_map


def get_acc(data):
  """gets avg. accuracy after each round of co-training for each experiment done
  avg took over 10 runs

  Arguments:
      data {dictionary} --  {'lvl_sup':[metrics], ...}

  Returns:
      list floats -- avg. acc. for each lvl. of supervision per each round co-tr
  """
  experiments_acc = []
  for d in data:
    experiment_acc = []
    for k, _ in d.items():
      experiment_acc.append(d[k][0][0])
    experiments_acc.append(experiment_acc)

  return experiments_acc


def get_conf_level_supervison(data):
  """returns the confidance levels computed for acc over 10 runs
  95% interval of confidance
  Arguments:
      data {dictionary} --  {'lvl_sup':[metrics], ...}

  Returns:
      list floats -- conf value for each lvl. of supervision
  """
  experiments_conf = []
  for d in data:
    experiment_conf = []
    for k, _ in d.items():
      experiment_conf.append(d[k][3])
    experiments_conf.append(experiment_conf)

  return experiments_conf


def get_std_level_supervison(data):
  """returns the standard deviation compute for acc over 10 runs

  Arguments:
      data {dictionary} --  {'lvl_sup':[metrics], ...}

  Returns:
      list floats -- std values for each lvl. of supervision
  """
  experiments_std = []
  for d in data:
    experiment_std = []
    for k, _ in d.items():
      experiment_std.append(d[k][4][0])
    experiments_std.append(experiment_std)

  return experiments_std


def get_min_max_level_supervison(data):
  """returns error from avg and min, max value from last round of co-tr.
  per each level of super

  Arguments:
    data {dictionary} --  {'lvl_sup':[metrics], ...}

  Returns:
    list floats -- result of error computing values for each lvl. of supervision
  """
  experiments_min_max = []

  for d in data:
    experiment_min_max = []
    for k, _ in d.items():
      experiment_min_max.append((abs(d[k][1][0] - d[k][0][0]),
                                 abs(d[k][2][0] - d[k][0][0])))
    experiments_min_max.append(experiment_min_max)

  return experiments_min_max
