"""
utils.py module contains various methods used for saving results, loading,
shuffling data
"""

import glob
import pickle
from collections import Counter
import numpy as np

from sklearn.metrics import accuracy_score, average_precision_score
from sklearn.metrics import precision_score, recall_score
from sklearn.preprocessing import LabelBinarizer


def load_compressed_features(dataset_folder, keyword='compressed'):
  """loads compressed/ modular data stored in pickeled files

  Arguments:
      dataset_folder {string} -- path to where data can be found
      keyword {string} -- accepted args 'compressed'/ 'modular'
                          default value='compressed'
  Returns:
      float32 -- features
      int32 --labels corresponednt to features
  """
  files = sorted(glob.glob("{}/{}/*".format(dataset_folder, keyword)))
  labels = []  #modular
  # features = None
  new_class_features = None
  for (indx, f) in enumerate(files):

    with open(f, 'rb') as in_file:
      new_features = pickle.load(in_file)

    if new_class_features is None:
      new_class_features = new_features
    else:
      new_class_features = np.append(new_class_features, new_features, axis=0)

    labels = labels + ([indx] * len(new_features))

  features = np.asarray(new_class_features)
  labels = np.asarray(labels, dtype=np.uint16)

  return features, labels


def load_extracted_features(dataset_folder):
  """loads pure extracted features, those have other forat than compressed ones

  Arguments:
      dataset_folder {string} -- path to where data can be found

  Returns:
      float32 -- features
      int32 --labels corresponednt to features
  """
  folders = sorted(glob.glob("{}/processed/*".format(dataset_folder)))
  classes_no = []
  features = None

  for folder in folders:
    new_class_features = None
    files = sorted(glob.glob("{}/*.features".format(folder)))
    for f in files:
      with open(f, 'rb') as in_file:
        new_features = pickle.load(in_file)

      if new_class_features is None:
        new_class_features = new_features
      else:
        new_class_features = np.append(new_class_features, new_features, axis=0)

    classes_no.append(len(new_class_features))
    if features is None:
      features = new_class_features
    else:
      features = np.append(features, new_class_features, axis=0)
  labels = []

  for (i, no) in enumerate(classes_no):
    new_labels = [i] * no
    labels += new_labels

  features = np.asarray(features)
  labels = np.asarray(labels)

  return features, labels


def shuffle_unison(features, labels, rng_state=None):
  """shuffles data with labels

  Arguments:
      features {float32} -- features images
      labels {int32} -- labels corr. features

  Keyword Arguments:
      rng_state {int} -- use random state (default: {None})
  """
  if rng_state is None:
    rng_state = np.random.get_state()

  np.random.set_state(rng_state)
  np.random.shuffle(features)
  np.random.set_state(rng_state)
  np.random.shuffle(labels)


def write_to_file(file_name, to_write, experiment_info):
  """writes to file data

  Arguments:
      file_name {string} -- path to file where to write info
      to_write {list} -- infor to be written
      experiment_info {string} -- information regarding the experiment
  """
  file = open(file_name, "a+")
  file.write(experiment_info + " " + to_write + "\n")
  file.close()


def read_checkpoint_file(file_name):
  """reads the checkpoint file in case of resoring

  Arguments:
      file_name {string} -- filenam path
  Returns:
      [float] -- last experiment done
  """
  file = open(file_name, "r")
  last_line = list(file)[-1]
  lst_line = last_line.split()
  file.close()

  return float(lst_line[1])


def read_results_file(file_name):
  """read from results file

  Arguments:
      file_name {string} -- path to file

  Returns:
      list -- 2 lists: acc per round, #fake labels added per each round
  """
  precentage_fake_labels = []
  acc_precentage_fake_labels = []

  file = open(file_name, "r")
  for line in file:
    split_line = line.split()
    if "acc" in split_line:
      indx = split_line.index("acc")
      precentage_fake_labels.append(float(split_line[0]))
      acc_precentage_fake_labels.append(float(split_line[indx + 1]))
  file.close()

  return acc_precentage_fake_labels, precentage_fake_labels


def get_class_weights(y):
  """returns computed class weights

  Arguments:
      y {int32} -- labels

  Returns:
      dictionary -- {cls:value ...}
  """

  counter = Counter(y)
  majority = max(counter.values())
  return {
      cls: round(float(majority) / float(count), 2)
      for cls, count in counter.items()
  }


def compute_multiclass_average_precision(predictions, ground_truth):
  """calculates map

  Arguments:
      predictions {int32} -- labels for predictions made
      ground_truth {int32} -- true labels

  Returns:
      float32 -- map
  """
  lb = LabelBinarizer()
  lb.fit(ground_truth)
  y_test = lb.transform(ground_truth)
  y_pred = lb.transform(predictions)
  return average_precision_score(y_test, y_pred)


def compute_metrics(predictions, ground_truth):
  """aggregates comp. multiple metrics

  Arguments:
      predictions {int32} -- labels for predictions made
      ground_truth {int32} -- true labels

  Returns:
      [float 32] -- multiple metrics computed
  """

  multiclass_average_precision = compute_multiclass_average_precision(
      predictions, ground_truth)
  p = precision_score(ground_truth, predictions, average='macro')
  r = recall_score(ground_truth, predictions, average='macro')
  acc = accuracy_score(ground_truth, predictions)
  error = 1 - acc

  return acc, error, p, r, multiclass_average_precision


def pickle_results(pickle_file, result_dict):
  """pickles given dictionary

  Arguments:
      pickle_file {string} -- where to save result of pickling
      result_dict {dictionary} -- given dictionary to store
  """
  f = open(pickle_file, 'wb')
  pickle.dump(result_dict, f)
  f.close()


def read_pickled_results(pickle_file):
  """read the pickled results

  Arguments:
      pickle_file {string} -- where to read from

  Returns:
      list -- metrics stored in pickles
  """
  metrics = []
  with open(pickle_file, 'rb') as f:
    res = pickle.load(f)

  metrics.append(res['acc_join'][-1])
  metrics.append(res['err_join'][-1])
  metrics.append(res['p_join'][-1])
  metrics.append(res['r_join'][-1])
  metrics.append(res['m_ap_join'][-1])

  return metrics


def read_pickled_results_est(pickle_file):
  """reads pickled results for baseline experiments; different format

  Arguments:
      pickle_file {string} -- where to read from

  Returns:
      list -- read metrics
  """
  metrics = []
  with open(pickle_file, 'rb') as f:
    res = pickle.load(f)
    # metrics.append(res['acc'])
    # metrics.append(res['err'])
    # metrics.append(res['p'])
    # metrics.append(res['r'])
    # metrics.append(res['m_ap'])

    metrics.append(res['acc'][-1])
    metrics.append(res['err'][-1])
    metrics.append(res['p'][-1])
    metrics.append(res['r'][-1])
    metrics.append(res['m_ap'][-1])

  return metrics
