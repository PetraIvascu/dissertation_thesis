"""
supervised_estimator-cpu.py config file xgb hyperparameters limited supervision

"""

from __future__ import absolute_import
import os

training_params = {
    "use_gpu": False,
    "restore": False,
    "num_classes": 102,
    "num_rounds": 10,
    "num_rounds_early_stopping": 4,
    "softmax_probability": False,
    "vgg":
    "C:/Users/ivasc/Documents/disertatie/datasets/caltech101/vgg_compressed",
    "revnet":
    "C:/Users/ivasc/Documents/disertatie/datasets/caltech/revnet_compressed",
    "resnet":
    "C:/Users/ivasc/Documents/disertatie/datasets/caltech101/resnet_compressed",
    "save_model_location": "output/",
    "history_training": "output/",
    "pickle_result": "output/",
    "restoring_file": os.path.join("output", "sl_checkpoint_restoring.txt"),
    "levels_supervision": [0.03, 0.04, 0.06, 0.12, 0.17, 0.23]
}

# "vgg_sup":
# "C:/Users/ivasc/Documents/disertatie/datasets/caltech/vgg/05-proc-supervision-vgg.pkl",
# "revnet_sup":
# "C:/Users/ivasc/Documents/disertatie/datasets/caltech/revnet/05-proc-supervision-revnet.pkl",
