"""
supervised_estimator-gpu.py config file xgb hyperparameters limited supervision
gpu

"""

from __future__ import absolute_import
import numpy as np
import os

training_params = {
    "use_gpu": True,
    "restore": False,
    "num_classes": 102,
    "num_rounds": 10,
    "num_rounds_early_stopping": 10,
    "softmax_probability": False,
    "vgg": "/home/ivascupetra/data_disertatie/vgg_compressed",
    "revnet": "/home/ivascupetra/data_disertatie/revnet",
    "resnet": "/home/ivascupetra/data_disertatie/resnet_compressed",
    "save_model_location": "output/",
    "history_training": "output/",
    "restoring_file": os.path.join("output", "sl_checkpoint_restoring.txt"),
    "levels_supervision": [0.03, 0.04, 0.06, 0.12, 0.17,
                           0.23]  # supervision used
}
