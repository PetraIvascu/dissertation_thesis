"""
co-training-gpu.py config file containig hyperparameters co-training for gpu
"""
from __future__ import absolute_import

import os

training_params = {
    "use_gpu": False,
    "restore": False,
    "num_classes": 102,
    "num_rounds": 10,
    "num_co_training_rounds": 10,
    "num_rounds_early_stopping": 3,
    "softmax_probability": True,
    "vgg": "/home/ivascu_maria1/data/caltech101/vgg/",
    "revnet": "/home/ivascu_maria1/data/revnet",
    "resnet": "/home/ivascu_maria1/data/caltech101/resnet/",
    "save_model_location_v1": "output/v1",
    "save_model_location_v2": "output/v2",
    "save_model_location_final": "output/",
    "history_training": "output/",
    "pickle_resulst": "output/",
    "restoring_file": os.path.join("output", "ssl_checkpoint_restoring.txt"),
    "levels_supervision": [0.03, 0.04, 0.06, 0.12, 0.17,
                           0.23]  # supervision used expressed in %
}