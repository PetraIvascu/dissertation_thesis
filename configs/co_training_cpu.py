"""
co-training-cpu.py config file containig hyperparameters co-training cpu

"""

from __future__ import absolute_import
import os

training_params = {
    "no_runs_experiments": 10,
    "use_gpu": False,
    "restore": False,
    "num_classes": 257,
    #102,
    "num_rounds": 10,
    "num_co_training_rounds": 20,  # 10 - for c101 20 - for c256
    "num_rounds_early_stopping": 3,
    "softmax_probability": True,
    "vgg":
    "C:/Users/ivasc/Documents/disertatie/datasets/caltech256/vgg_compressed",
    "resnet":
    "C:/Users/ivasc/Documents/disertatie/datasets/caltech256/resnet_compressed",
    "save_model_location_v1": "output\\v1",  # v1_05sup_vgg.xgb
    "save_model_location_v2": "output\\v2",  # v2_05sup_revnet.xgb
    "save_model_location_final": "output\\",
    "history_training": "output\\",
    "pickle_resulst": "output\\",
    "restoring_file": os.path.join("output", "ssl_checkpoint_restoring.txt"),
    "levels_supervision": [0.02]
}
